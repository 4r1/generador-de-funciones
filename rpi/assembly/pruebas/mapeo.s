	.arch armv6
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.file	"mapeo.c"
	.section	.text.startup,"ax",%progbits
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu vfp
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r4, lr}
	sub	sp, sp, #8
	ldr	r1, .L3
	ldr	r0, .L3+4
	bl	open
	ldr	r3, .L3+8
	mov	r2, #3
	str	r3, [sp, #4]
	mov	r1, #4096
	mov	r3, #1
	mov	r4, r0
	str	r0, [sp]
	mov	r0, #0
	bl	mmap
	ldr	r3, .L3+12
	str	r0, [r3]
	mov	r0, r4
	add	sp, sp, #8
	@ sp needed
	pop	{r4, lr}
	b	close
.L4:
	.align	2
.L3:
	.word	1052674
	.word	.LC1
	.word	538968064
	.word	gpio
	.size	main, .-main
	.comm	gpio,4,4
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC1:
	.ascii	"/dev/mem\000"
	.ident	"GCC: (Raspbian 6.3.0-18+rpi1+deb9u1) 6.3.0 20170516"
	.section	.note.GNU-stack,"",%progbits
