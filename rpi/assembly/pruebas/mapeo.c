#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define BCM2835_PERI_BASE 	0x20000000
#define GPIO_BASE		(BCM2835_PERI_BASE + 0x200000)
#define BLOCK_SIZE		4096

#define GPFSEL	((volatile unsigned int *) (gpio + 0))
#define GPSET	((volatile unsigned int *) (gpio + 7))
#define GPCLR	((volatile unsigned int *) (gpio + 10))
#define GPLEV	((volatile unsigned int *) (gpio + 13))
#define INPUT	0
#define OUTPUT	1

volatile unsigned int *gpio;

void main(void){
	int mem_fd;
	void *reg_map;

	mem_fd = open("/dev/mem", O_RDWR|O_SYNC);
	reg_map = mmap(
		NULL,
		BLOCK_SIZE,
		PROT_READ|PROT_WRITE,
		MAP_SHARED,
		mem_fd,
		GPIO_BASE
	);

	gpio = (volatile unsigned *) reg_map;

	int pines[] = {0, 1, 5, 6 , 12, 13, 19, 16, 26, 20 ,21};

//	for(int i=0; i<11; i++){
//		int reg = pines[i]/10;
//		int off = (pines[i]%10)*3;
//		GPFSEL[reg] &= ~((0b111 & ~OUTPUT ) << off);
//		GPFSEL[reg] |= ((0b111 & OUTPUT ) << off);
//		reg = pines[i]/32;
//		off = pines[i]%32;
//		GPSET[reg] = 1 << off;
//		//GPCLR[reg] = 1 << off;
//	}
	int reg = 26/10;
	int off = (26%10)*3;

	close(mem_fd);
}
