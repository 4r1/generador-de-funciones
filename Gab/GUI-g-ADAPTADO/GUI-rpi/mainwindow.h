﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define PI 3.14159265
//#define c_muestras 1000
//#define c_muestras_arb 80
#define c_muestras_1ms 3333
#define limitem_0 3
#define limites_0 200
//#define limitei_0

#include <QMainWindow>
#include <QMouseEvent>
#include "opciones.h"
#include "selondas.h"
#include "menuopc.h"
#include <bitset>
#include <QTimer>
#include <cstdlib>

typedef struct str_config {
    float vpp;
    float offset;
    float freq;
    int freq_mult;
    int onda; //0=seno,1=cuadrada,2=triangular,3=arbitraria
    QVector<double> valores_arb;
    QVector<double> valores_arb_x;

    str_config(float def_vpp=0,float def_offset=0,float def_freq=0,int def_freq_mult=0,int def_onda=0) {
        vpp=def_vpp;
        offset=def_offset;
        freq=def_freq;
        onda=def_onda;
        freq_mult=def_freq_mult;
    }

    struct str_config leer_mem(void);
    void escribir_mem(struct str_config *actual);
} config;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    config configuracion_act;
    int act_estado;
    float limx;
    float limy;
    float lim_tiempo;
    int arb_rango;
    double Vmin,Vmax;
    QFile *escritura_valores;
    QFile *escritura_valores2;
    QFile *lectura_estado_act;
    int punt_e;

    opciones *opcVppOffset;
    opciones *opcFreq;
    opciones *opcdivx;
    opciones *opcdivy;
    selOndas *sOndas;
    menuOpc *mOpc;
    QTimer *puntitos;

    void adaptar_todo(void);

    //double c_muestras;
    void actualizar(void);
    void acomodar_limx(void);
    void acomodar_limy(void);
    void mod_arb(bool estado);
    float valory(double valorx);
    //void normalizar_conf(config* c_norm);
    void definir_muestras(void);
    void verificar_limite(void); //sadasdasfasfda

    int c_muestras;
    int c_muestras_arb;
    int cantidad_muestras_total;

private slots:
    void on_pBVpp_clicked();

    void on_pBfreq_clicked();

    void on_pBond_clicked();

    void on_MainWindow_destroyed();

    void mouseReleaseEvent(QMouseEvent *event);

    void on_pBdivX_clicked();
    void mousePress(QMouseEvent* evento);
    void mover_graph(int estado,QMouseEvent* conecm);
    void mousMove(QMouseEvent* conecs);
    void mousRelease(QMouseEvent* conecs);
    void punt(void);

    void on_pBopc_clicked();

    void on_pBdivY_clicked();

public slots:
    void leer_valores(int sel);
    void enfocar(void);
    void tyv_valores(void);
    void restablecer_arb(void);
    void enviandoValores(void);
    void toggle_label_espera(bool qua);
    void llamar_tle();

private:
    Ui::MainWindow *ui;

signals:
    void llamar_act(int estado, QMouseEvent* conec);
    void sig_togg_le(bool que);
};

#endif // MAINWINDOW_H
