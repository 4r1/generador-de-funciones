#include "menuopc.h"
#include "ui_menuopc.h"

menuOpc::menuOpc(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::menuOpc)
{
    ui->setupUi(this);
}

menuOpc::~menuOpc()
{
    delete ui;
}

void menuOpc::on_pBAct_clicked()
{
    emit enviar_valores();
}

void menuOpc::on_pBRest_clicked()
{
    emit restablecer_arb();
}
