#ifndef SELONDAS_H
#define SELONDAS_H

#include <QWidget>

namespace Ui {
class selOndas;
}

class selOndas : public QWidget
{
    Q_OBJECT

public:
    explicit selOndas(QWidget *parent = nullptr);
    ~selOndas();
    int tipo_onda;

private slots:
    void on_pBond_3_clicked();

    void on_pBond_4_clicked();

    void on_pBond_2_clicked();

    void on_pBond_5_clicked();

signals:
    void valor_cambiado (int sal);

private:
    Ui::selOndas *ui;
};

#endif // SELONDAS_H
