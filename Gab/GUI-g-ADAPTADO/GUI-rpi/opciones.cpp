#include "opciones.h"
#include "ui_opciones.h"

opciones::opciones(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::opciones)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowTitleHint);
    setWindowTitle(" ");
    ui->pushButton->setVisible(false);
    ui->pBFS->setVisible(false);
    ui->PBFB->setVisible(false);
    ui->lEMF->setVisible(false);
    ui->pBCS->setVisible(false);
    ui->PBCB->setVisible(false);
    ui->lEC->setVisible(false);
    ui->lEC2->setVisible(false);
    ui->lg1->setVisible(false);
}

void opciones::inic(int opc)
{
    mod_act=opc;

    switch (opc) {
    case 0: {
        ui->pushButton->setVisible(true);
        ui->lbSl1->setText("Vpp");
        ui->lbSl2->setText("Offset");
        ui->Sl1->setRange(0,200);
        ui->Sl2->setRange(-100,100);
        ui->Sl1->setValue(valor1);
        ui->Sl2->setValue(valor2);
        break;
    }
    case 1: {
        ui->lbSl1->setText("Freq");
        ui->lbSl2->setVisible(false);
        ui->Sl2->setVisible(false);
        ui->pushButton->setVisible(false);
        ui->pBFS->setVisible(true);
        ui->PBFB->setVisible(true);
        ui->lEMF->setVisible(true);
        ui->pBCS->setVisible(true);
        ui->PBCB->setVisible(true);
        ui->lEC->setVisible(true);
        ui->lEC2->setVisible(true);
        ui->lg1->setVisible(true);
        setFixedWidth(100);
        break;
    }
    case 2: {
        ui->lbSl1->setText("tmax");
        ui->Sl1->setRange(1,199);
        ui->lbSl2->setVisible(false);
        ui->Sl2->setVisible(false);
        ui->pushButton->setVisible(false);
        setFixedWidth(80);
        break;
    }
    case 3: {
        ui->lbSl1->setText("Vmax");
        ui->lbSl2->setVisible(false);
        ui->Sl2->setVisible(false);
        setFixedWidth(80);
        break;
    }
    }
}

int opciones::leer_valor(bool sel)
{
    if(sel==0)
        return ui->Sl1->value();

    return ui->Sl2->value();
}

int opciones::leer_mult()
{
    switch (ui->lEMF->text().toStdString().c_str()[0]) {
    case 'H': {
        return 0;
    }
    case 'K': {
        return 3;
    }
    case 'M': {
        return 6;
    }
    case 'G': {
        return 9;
    }
    }

    return 0;
}

void opciones::cargar_valor(int avalor1)
{
    valor1=avalor1;

    ui->Sl1->setValue(valor1);
}

void opciones::cargar_valor(int avalor1, int avalor2)
{
    valor1=avalor1;
    valor2=avalor2;

    ui->Sl1->setValue(valor1);
    ui->Sl2->setValue(valor2);
}

void opciones::cargar_valor(int avalor1, char esc)
{
    int mult=1,valortemp;

    valor1=avalor1;
    valortemp=valor1;

    if(valor1<100){
        ui->Sl1->setRange(1,99);
        mult=1;
    }
    else if (valor1<200) {
        ui->Sl1->setRange(100,199);
        mult=2;
    }
    else if (valor1<300) {
        ui->Sl1->setRange(200,299);
        mult=3;
    }
    else if (valor1<400) {
        ui->Sl1->setRange(300,399);
        mult=4;
    }
    else if (valor1<500) {
        ui->Sl1->setRange(400,499);
        mult=5;
    }
    else if (valor1<600) {
        ui->Sl1->setRange(500,599);
        mult=6;
    }
    else if (valor1<700) {
        ui->Sl1->setRange(600,699);
        mult=7;
    }
    else if (valor1<800) {
        ui->Sl1->setRange(700,799);
        mult=8;
    }
    else if (valor1<900) {
        ui->Sl1->setRange(800,899);
        mult=9;
    }
    else if (valor1<1000) {
        ui->Sl1->setRange(900,999);
        mult=10;
    }

    ui->lEC->setText(QString::number(100*(mult-1)));
    ui->lEC2->setText(QString::number(100*(mult)));

    ui->Sl1->setValue(valortemp);


    switch (esc) {
    case 'H': {
        ui->lEMF->setText("Hz");
        break;
    }
    case 'K': {
        ui->lEMF->setText("KHz");
        break;
    }
    case 'M': {
        ui->lEMF->setText("MHz");
        break;
    }
    case 'G': {
        ui->lEMF->setText("GHz");
        break;
    }
    }
}

void opciones::cargar_valor_fre(int avalor1, int sel)
{
    int mult=1,valortemp;

    valor1=avalor1;
    valortemp=valor1;

    if(valor1<100){
        ui->Sl1->setRange(1,99);
        mult=1;
    }
    else if (valor1<200) {
        ui->Sl1->setRange(100,199);
        mult=2;
    }
    else if (valor1<300) {
        ui->Sl1->setRange(200,299);
        mult=3;
    }
    else if (valor1<400) {
        ui->Sl1->setRange(300,399);
        mult=4;
    }
    else if (valor1<500) {
        ui->Sl1->setRange(400,499);
        mult=5;
    }
    else if (valor1<600) {
        ui->Sl1->setRange(500,599);
        mult=6;
    }
    else if (valor1<700) {
        ui->Sl1->setRange(600,699);
        mult=7;
    }
    else if (valor1<800) {
        ui->Sl1->setRange(700,799);
        mult=8;
    }
    else if (valor1<900) {
        ui->Sl1->setRange(800,899);
        mult=9;
    }
    else if (valor1<1000) {
        ui->Sl1->setRange(900,999);
        mult=10;
    }

    ui->lEC->setText(QString::number(100*(mult-1)));
    ui->lEC2->setText(QString::number(100*(mult)));

    ui->Sl1->setValue(valortemp);


    switch (sel) {
    case 0: {
        ui->lEMF->setText("Hz");
        break;
    }
    case 3: {
        ui->lEMF->setText("KHz");
        break;
    }
    case 6: {
        ui->lEMF->setText("MHz");
        break;
    }
    case 9: {
        ui->lEMF->setText("GHz");
        break;
    }
    }
}

opciones::~opciones()
{
    delete ui;
}

void opciones::on_Sl2_valueChanged(int value)
{
    valor2=value;

    emit valor_cambiado(mod_act);
}

void opciones::on_Sl1_valueChanged(int value)
{
    valor1=value;

    emit valor_cambiado(mod_act);
}

void opciones::on_pushButton_clicked()
{
    ui->Sl2->setValue(0);
}

void opciones::on_pBFS_clicked()
{
    switch (ui->lEMF->text().toStdString().c_str()[0]) {
    case 'H': {
        ui->lEMF->setText("KHz");
        break;
    }
    case 'K': {
        ui->lEMF->setText("MHz");
        break;
    }
    case 'M': {
        ui->lEMF->setText("GHz");
        break;
    }
    }

    emit valor_cambiado(mod_act);
}

void opciones::on_PBFB_clicked()
{
    switch (ui->lEMF->text().toStdString().c_str()[0]) {
    case 'K': {
        ui->lEMF->setText("Hz");
        break;
    }
    case 'M': {
        ui->lEMF->setText("KHz");
        break;
    }
    case 'G': {
        ui->lEMF->setText("MHz");
        break;
    }
    }

    emit valor_cambiado(mod_act);
}

void opciones::on_pBCS_clicked()
{
    int mult=1;

    if(ui->Sl1->value() < 100)
        mult=2;
    else if (ui->Sl1->value() < 200)
        mult=3;
    else if (ui->Sl1->value() < 300)
        mult=4;
    else if (ui->Sl1->value() < 400)
        mult=5;
    else if (ui->Sl1->value() < 500)
        mult=6;
    else if (ui->Sl1->value() < 600)
        mult=7;
    else if (ui->Sl1->value() < 700)
        mult=8;
    else if (ui->Sl1->value() < 800)
        mult=9;
    else if (ui->Sl1->value() < 900)
        mult=10;

    ui->lEC->setText(QString::number(100*(mult-1)));
    ui->lEC2->setText(QString::number(100*(mult)));
    ui->Sl1->setRange(100*(mult-1),100*mult-1);
    ui->Sl1->setValue(100*(mult-1));
}

void opciones::on_PBCB_clicked()
{
    int mult=1;

    if (ui->Sl1->value() < 200)
        mult=1;
    else if (ui->Sl1->value() < 300)
        mult=2;
    else if (ui->Sl1->value() < 400)
        mult=3;
    else if (ui->Sl1->value() < 500)
        mult=4;
    else if (ui->Sl1->value() < 600)
        mult=5;
    else if (ui->Sl1->value() < 700)
        mult=6;
    else if (ui->Sl1->value() < 800)
        mult=7;
    else if (ui->Sl1->value() < 900)
        mult=8;
    else if (ui->Sl1->value() < 1000)
        mult=8;

    ui->lEC->setText(QString::number(100*(mult-1)));
    ui->lEC2->setText(QString::number(100*(mult)));
    if(mult==1)
        ui->Sl1->setRange(100*(mult-1)+1,100*mult-1);
    else
        ui->Sl1->setRange(100*(mult-1),100*mult-1);

    ui->Sl1->setValue(100*mult-1);
}
