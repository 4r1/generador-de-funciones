/****************************************************************************
** Meta object code from reading C++ file 'opciones.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../GUI-rpi/opciones.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'opciones.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_opciones_t {
    QByteArrayData data[12];
    char stringdata0[162];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_opciones_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_opciones_t qt_meta_stringdata_opciones = {
    {
QT_MOC_LITERAL(0, 0, 8), // "opciones"
QT_MOC_LITERAL(1, 9, 14), // "valor_cambiado"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 4), // "mact"
QT_MOC_LITERAL(4, 30, 19), // "on_Sl2_valueChanged"
QT_MOC_LITERAL(5, 50, 5), // "value"
QT_MOC_LITERAL(6, 56, 19), // "on_Sl1_valueChanged"
QT_MOC_LITERAL(7, 76, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(8, 98, 15), // "on_pBFS_clicked"
QT_MOC_LITERAL(9, 114, 15), // "on_PBFB_clicked"
QT_MOC_LITERAL(10, 130, 15), // "on_pBCS_clicked"
QT_MOC_LITERAL(11, 146, 15) // "on_PBCB_clicked"

    },
    "opciones\0valor_cambiado\0\0mact\0"
    "on_Sl2_valueChanged\0value\0on_Sl1_valueChanged\0"
    "on_pushButton_clicked\0on_pBFS_clicked\0"
    "on_PBFB_clicked\0on_pBCS_clicked\0"
    "on_PBCB_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_opciones[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   57,    2, 0x08 /* Private */,
       6,    1,   60,    2, 0x08 /* Private */,
       7,    0,   63,    2, 0x08 /* Private */,
       8,    0,   64,    2, 0x08 /* Private */,
       9,    0,   65,    2, 0x08 /* Private */,
      10,    0,   66,    2, 0x08 /* Private */,
      11,    0,   67,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void opciones::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        opciones *_t = static_cast<opciones *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->valor_cambiado((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_Sl2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_Sl1_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_pushButton_clicked(); break;
        case 4: _t->on_pBFS_clicked(); break;
        case 5: _t->on_PBFB_clicked(); break;
        case 6: _t->on_pBCS_clicked(); break;
        case 7: _t->on_PBCB_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (opciones::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&opciones::valor_cambiado)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject opciones::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_opciones.data,
      qt_meta_data_opciones,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *opciones::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *opciones::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_opciones.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int opciones::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void opciones::valor_cambiado(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
