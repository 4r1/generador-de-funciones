/********************************************************************************
** Form generated from reading UI file 'opciones.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPCIONES_H
#define UI_OPCIONES_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_opciones
{
public:
    QSlider *Sl1;
    QSlider *Sl2;
    QLabel *lbSl1;
    QLabel *lbSl2;
    QPushButton *pushButton;
    QPushButton *pBFS;
    QPushButton *PBFB;
    QLabel *label;
    QLabel *lEMF;
    QPushButton *pBCS;
    QPushButton *PBCB;
    QLabel *lEC;
    QLabel *lg1;
    QLabel *lEC2;

    void setupUi(QWidget *opciones)
    {
        if (opciones->objectName().isEmpty())
            opciones->setObjectName(QStringLiteral("opciones"));
        opciones->resize(128, 200);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(opciones->sizePolicy().hasHeightForWidth());
        opciones->setSizePolicy(sizePolicy);
        opciones->setFocusPolicy(Qt::StrongFocus);
        opciones->setWindowOpacity(0.5);
        opciones->setAutoFillBackground(false);
        opciones->setStyleSheet(QStringLiteral("background: rgba(0,0,0,130)"));
        Sl1 = new QSlider(opciones);
        Sl1->setObjectName(QStringLiteral("Sl1"));
        Sl1->setGeometry(QRect(25, 28, 12, 172));
        Sl1->setStyleSheet(QStringLiteral("background: rgba(70,0,0,100)"));
        Sl1->setOrientation(Qt::Vertical);
        Sl2 = new QSlider(opciones);
        Sl2->setObjectName(QStringLiteral("Sl2"));
        Sl2->setGeometry(QRect(88, 28, 12, 172));
        Sl2->setStyleSheet(QStringLiteral("background: rgba(70,0,0,100)"));
        Sl2->setOrientation(Qt::Vertical);
        lbSl1 = new QLabel(opciones);
        lbSl1->setObjectName(QStringLiteral("lbSl1"));
        lbSl1->setGeometry(QRect(9, 1, 42, 26));
        QPalette palette;
        QBrush brush(QColor(238, 238, 236, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 0, 0, 15));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        QBrush brush2(QColor(190, 190, 190, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        lbSl1->setPalette(palette);
        QFont font;
        font.setPointSize(9);
        lbSl1->setFont(font);
        lbSl1->setAutoFillBackground(false);
        lbSl1->setStyleSheet(QStringLiteral("background: rgba(0,0,0,15)"));
        lbSl1->setFrameShape(QFrame::NoFrame);
        lbSl1->setFrameShadow(QFrame::Plain);
        lbSl1->setLineWidth(0);
        lbSl1->setTextFormat(Qt::AutoText);
        lbSl1->setAlignment(Qt::AlignCenter);
        lbSl2 = new QLabel(opciones);
        lbSl2->setObjectName(QStringLiteral("lbSl2"));
        lbSl2->setGeometry(QRect(74, 1, 42, 26));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        lbSl2->setPalette(palette1);
        lbSl2->setFont(font);
        lbSl2->setStyleSheet(QStringLiteral("background: rgba(0,0,0,15)"));
        lbSl2->setAlignment(Qt::AlignCenter);
        pushButton = new QPushButton(opciones);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(72, 104, 16, 10));
        QPalette palette2;
        QBrush brush3(QColor(255, 255, 255, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        QBrush brush4(QColor(0, 0, 0, 130));
        brush4.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush4);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush4);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush4);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush4);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush4);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush4);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush4);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush4);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush4);
        pushButton->setPalette(palette2);
        QFont font1;
        font1.setFamily(QStringLiteral("Ubuntu"));
        font1.setPointSize(9);
        pushButton->setFont(font1);
        pushButton->setFlat(true);
        pBFS = new QPushButton(opciones);
        pBFS->setObjectName(QStringLiteral("pBFS"));
        pBFS->setGeometry(QRect(40, 40, 20, 20));
        QIcon icon;
        icon.addFile(QStringLiteral(":/fAr.png"), QSize(), QIcon::Normal, QIcon::Off);
        pBFS->setIcon(icon);
        pBFS->setIconSize(QSize(20, 20));
        pBFS->setFlat(true);
        PBFB = new QPushButton(opciones);
        PBFB->setObjectName(QStringLiteral("PBFB"));
        PBFB->setGeometry(QRect(40, 73, 20, 20));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/fAb.png"), QSize(), QIcon::Normal, QIcon::Off);
        PBFB->setIcon(icon1);
        PBFB->setIconSize(QSize(20, 20));
        PBFB->setFlat(true);
        label = new QLabel(opciones);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 128, 200));
        label->setAutoFillBackground(false);
        label->setStyleSheet(QStringLiteral("background: rgba(0,0,0,130)"));
        lEMF = new QLabel(opciones);
        lEMF->setObjectName(QStringLiteral("lEMF"));
        lEMF->setGeometry(QRect(40, 60, 32, 14));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush5(QColor(0, 0, 0, 250));
        brush5.setStyle(Qt::SolidPattern);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush5);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush5);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush5);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush5);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush5);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush5);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush5);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush5);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush5);
        lEMF->setPalette(palette3);
        lEMF->setFont(font);
        lEMF->setAutoFillBackground(false);
        lEMF->setStyleSheet(QStringLiteral("background: rgba(0,0,0,250)"));
        lEMF->setFrameShape(QFrame::Box);
        lEMF->setFrameShadow(QFrame::Plain);
        lEMF->setLineWidth(1);
        lEMF->setTextFormat(Qt::AutoText);
        lEMF->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lEMF->setWordWrap(false);
        pBCS = new QPushButton(opciones);
        pBCS->setObjectName(QStringLiteral("pBCS"));
        pBCS->setGeometry(QRect(40, 106, 20, 20));
        pBCS->setIcon(icon);
        pBCS->setIconSize(QSize(20, 20));
        pBCS->setFlat(true);
        PBCB = new QPushButton(opciones);
        PBCB->setObjectName(QStringLiteral("PBCB"));
        PBCB->setGeometry(QRect(40, 162, 20, 20));
        PBCB->setIcon(icon1);
        PBCB->setIconSize(QSize(20, 20));
        PBCB->setFlat(true);
        lEC = new QLabel(opciones);
        lEC->setObjectName(QStringLiteral("lEC"));
        lEC->setGeometry(QRect(40, 126, 32, 14));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush5);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush5);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush5);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush5);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush5);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush5);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush5);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush5);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush5);
        lEC->setPalette(palette4);
        QFont font2;
        font2.setPointSize(9);
        font2.setBold(false);
        font2.setWeight(50);
        lEC->setFont(font2);
        lEC->setAutoFillBackground(false);
        lEC->setStyleSheet(QStringLiteral("background: rgba(0,0,0,250)"));
        lEC->setFrameShape(QFrame::Box);
        lEC->setFrameShadow(QFrame::Plain);
        lEC->setLineWidth(1);
        lEC->setTextFormat(Qt::AutoText);
        lEC->setAlignment(Qt::AlignCenter);
        lEC->setWordWrap(false);
        lg1 = new QLabel(opciones);
        lg1->setObjectName(QStringLiteral("lg1"));
        lg1->setGeometry(QRect(48, 138, 16, 10));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush6(QColor(0, 0, 0, 0));
        brush6.setStyle(Qt::SolidPattern);
        palette5.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush6);
        lg1->setPalette(palette5);
        lg1->setFont(font);
        lg1->setAutoFillBackground(false);
        lg1->setStyleSheet(QStringLiteral("background: rgba(0,0,0,0)"));
        lg1->setFrameShape(QFrame::NoFrame);
        lg1->setFrameShadow(QFrame::Plain);
        lg1->setLineWidth(0);
        lg1->setTextFormat(Qt::AutoText);
        lg1->setAlignment(Qt::AlignCenter);
        lEC2 = new QLabel(opciones);
        lEC2->setObjectName(QStringLiteral("lEC2"));
        lEC2->setGeometry(QRect(40, 149, 32, 14));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Active, QPalette::Button, brush5);
        palette6.setBrush(QPalette::Active, QPalette::Base, brush5);
        palette6.setBrush(QPalette::Active, QPalette::Window, brush5);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush5);
        palette6.setBrush(QPalette::Inactive, QPalette::Base, brush5);
        palette6.setBrush(QPalette::Inactive, QPalette::Window, brush5);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush5);
        palette6.setBrush(QPalette::Disabled, QPalette::Base, brush5);
        palette6.setBrush(QPalette::Disabled, QPalette::Window, brush5);
        lEC2->setPalette(palette6);
        lEC2->setFont(font);
        lEC2->setAutoFillBackground(false);
        lEC2->setStyleSheet(QStringLiteral("background: rgba(0,0,0,250)"));
        lEC2->setFrameShape(QFrame::Box);
        lEC2->setFrameShadow(QFrame::Plain);
        lEC2->setLineWidth(1);
        lEC2->setTextFormat(Qt::AutoText);
        lEC2->setAlignment(Qt::AlignCenter);
        lEC2->setWordWrap(false);
        label->raise();
        Sl1->raise();
        Sl2->raise();
        lbSl1->raise();
        lbSl2->raise();
        pushButton->raise();
        pBFS->raise();
        PBFB->raise();
        lEMF->raise();
        pBCS->raise();
        PBCB->raise();
        lEC->raise();
        lg1->raise();
        lEC2->raise();

        retranslateUi(opciones);

        QMetaObject::connectSlotsByName(opciones);
    } // setupUi

    void retranslateUi(QWidget *opciones)
    {
        opciones->setWindowTitle(QApplication::translate("opciones", "Vpp - Offset", nullptr));
        lbSl1->setText(QApplication::translate("opciones", "Vpp", nullptr));
        lbSl2->setText(QApplication::translate("opciones", "Offset", nullptr));
        pushButton->setText(QApplication::translate("opciones", "-", nullptr));
        pBFS->setText(QString());
        PBFB->setText(QString());
        label->setText(QString());
        lEMF->setText(QApplication::translate("opciones", "Hz", nullptr));
        pBCS->setText(QString());
        PBCB->setText(QString());
        lEC->setText(QApplication::translate("opciones", "0", nullptr));
        lg1->setText(QApplication::translate("opciones", "-", nullptr));
        lEC2->setText(QApplication::translate("opciones", "100", nullptr));
    } // retranslateUi

};

namespace Ui {
    class opciones: public Ui_opciones {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPCIONES_H
