/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../GUI-rpi/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[26];
    char stringdata0[302];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 10), // "llamar_act"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 6), // "estado"
QT_MOC_LITERAL(4, 30, 12), // "QMouseEvent*"
QT_MOC_LITERAL(5, 43, 5), // "conec"
QT_MOC_LITERAL(6, 49, 16), // "on_pBVpp_clicked"
QT_MOC_LITERAL(7, 66, 17), // "on_pBfreq_clicked"
QT_MOC_LITERAL(8, 84, 16), // "on_pBond_clicked"
QT_MOC_LITERAL(9, 101, 23), // "on_MainWindow_destroyed"
QT_MOC_LITERAL(10, 125, 17), // "mouseReleaseEvent"
QT_MOC_LITERAL(11, 143, 5), // "event"
QT_MOC_LITERAL(12, 149, 17), // "on_pBdivX_clicked"
QT_MOC_LITERAL(13, 167, 10), // "mousePress"
QT_MOC_LITERAL(14, 178, 6), // "evento"
QT_MOC_LITERAL(15, 185, 11), // "mover_graph"
QT_MOC_LITERAL(16, 197, 6), // "conecm"
QT_MOC_LITERAL(17, 204, 8), // "mousMove"
QT_MOC_LITERAL(18, 213, 6), // "conecs"
QT_MOC_LITERAL(19, 220, 11), // "mousRelease"
QT_MOC_LITERAL(20, 232, 16), // "on_pBopc_clicked"
QT_MOC_LITERAL(21, 249, 12), // "leer_valores"
QT_MOC_LITERAL(22, 262, 3), // "sel"
QT_MOC_LITERAL(23, 266, 7), // "enfocar"
QT_MOC_LITERAL(24, 274, 11), // "tyv_valores"
QT_MOC_LITERAL(25, 286, 15) // "restablecer_arb"

    },
    "MainWindow\0llamar_act\0\0estado\0"
    "QMouseEvent*\0conec\0on_pBVpp_clicked\0"
    "on_pBfreq_clicked\0on_pBond_clicked\0"
    "on_MainWindow_destroyed\0mouseReleaseEvent\0"
    "event\0on_pBdivX_clicked\0mousePress\0"
    "evento\0mover_graph\0conecm\0mousMove\0"
    "conecs\0mousRelease\0on_pBopc_clicked\0"
    "leer_valores\0sel\0enfocar\0tyv_valores\0"
    "restablecer_arb"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   94,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   99,    2, 0x08 /* Private */,
       7,    0,  100,    2, 0x08 /* Private */,
       8,    0,  101,    2, 0x08 /* Private */,
       9,    0,  102,    2, 0x08 /* Private */,
      10,    1,  103,    2, 0x08 /* Private */,
      12,    0,  106,    2, 0x08 /* Private */,
      13,    1,  107,    2, 0x08 /* Private */,
      15,    2,  110,    2, 0x08 /* Private */,
      17,    1,  115,    2, 0x08 /* Private */,
      19,    1,  118,    2, 0x08 /* Private */,
      20,    0,  121,    2, 0x08 /* Private */,
      21,    1,  122,    2, 0x0a /* Public */,
      23,    0,  125,    2, 0x0a /* Public */,
      24,    0,  126,    2, 0x0a /* Public */,
      25,    0,  127,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 4,    3,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,   11,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,   14,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 4,    3,   16,
    QMetaType::Void, 0x80000000 | 4,   18,
    QMetaType::Void, 0x80000000 | 4,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->llamar_act((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QMouseEvent*(*)>(_a[2]))); break;
        case 1: _t->on_pBVpp_clicked(); break;
        case 2: _t->on_pBfreq_clicked(); break;
        case 3: _t->on_pBond_clicked(); break;
        case 4: _t->on_MainWindow_destroyed(); break;
        case 5: _t->mouseReleaseEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 6: _t->on_pBdivX_clicked(); break;
        case 7: _t->mousePress((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 8: _t->mover_graph((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QMouseEvent*(*)>(_a[2]))); break;
        case 9: _t->mousMove((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 10: _t->mousRelease((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 11: _t->on_pBopc_clicked(); break;
        case 12: _t->leer_valores((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->enfocar(); break;
        case 14: _t->tyv_valores(); break;
        case 15: _t->restablecer_arb(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MainWindow::*)(int , QMouseEvent * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::llamar_act)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::llamar_act(int _t1, QMouseEvent * _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
