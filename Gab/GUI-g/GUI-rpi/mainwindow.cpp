#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    
    move(0,0);

    opcVppOffset=new opciones(this);
    opcFreq=new opciones(this);
    opcdivx=new opciones(this);
    opcdivy=new opciones(this);
    sOndas=new selOndas(this);
    mOpc=new menuOpc(this);

    opcVppOffset->hide();
    opcFreq->hide();
    opcdivx->hide();
    opcdivy->hide();
    sOndas->hide();
    mOpc->hide();

    act_estado=0;

    connect(opcVppOffset,SIGNAL(valor_cambiado(int)),this,SLOT(leer_valores(int)));
    connect(opcFreq,SIGNAL(valor_cambiado(int)),this,SLOT(leer_valores(int)));
    connect(opcdivx,SIGNAL(valor_cambiado(int)),this,SLOT(leer_valores(int)));
    connect(opcdivy,SIGNAL(valor_cambiado(int)),this,SLOT(leer_valores(int)));
    connect(sOndas,SIGNAL(valor_cambiado(int)), this, SLOT(leer_valores(int)));
    //connect(this,SIGNAL(mouseReleaseEvent()),this,SLOT(enfocar()));
    connect(ui->grafico, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress(QMouseEvent*)));
    connect(ui->grafico, SIGNAL(mouseMove(QMouseEvent*)),this, SLOT(mousMove(QMouseEvent*)));
    connect(ui->grafico, SIGNAL(mouseRelease(QMouseEvent*)),this, SLOT(mousRelease(QMouseEvent*)));
    connect(this, SIGNAL(llamar_act(int,QMouseEvent*)),this, SLOT(mover_graph(int,QMouseEvent*)));
    connect(mOpc,SIGNAL(enviar_valores()),this,SLOT(tyv_valores()));
    connect(mOpc,SIGNAL(restablecer_arb()),this,SLOT(restablecer_arb()));

    opcVppOffset->inic(0);
    opcVppOffset->move(0,130);
    opcFreq->inic(1);
    opcFreq->move(161,130);
    opcdivx->inic(2);
    opcdivx->move(351,130);
    opcdivy->inic(3);
    opcdivy->move(161,130);
    sOndas->move(241,70);
    mOpc->move(439,232);


    ui->grafico->xAxis->setBasePen(QPen(Qt::blue,1));
    ui->grafico->yAxis->setBasePen(QPen(Qt::blue,1));
    ui->grafico->xAxis->setTickPen(QPen(Qt::blue, 1));
    ui->grafico->yAxis->setTickPen(QPen(Qt::blue, 1));
    ui->grafico->xAxis->setSubTickPen(QPen(Qt::blue, 1));
    ui->grafico->yAxis->setSubTickPen(QPen(Qt::blue, 1));
    ui->grafico->yAxis->axisRect()->setAutoMargins(QCP::msNone);
    ui->grafico->yAxis->axisRect()->setMargins(QMargins(70,15,15,45));
    ui->grafico->yAxis->setTickLabelPadding(5);
//    ui->grafico->xAxis->setTickLabelColor(Qt::white);
//    ui->grafico->yAxis->setTickLabelColor(Qt::white);
    ui->grafico->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->grafico->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->grafico->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->grafico->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->grafico->xAxis->grid()->setSubGridVisible(true);
    ui->grafico->yAxis->grid()->setSubGridVisible(true);
    ui->grafico->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->grafico->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->grafico->xAxis->setLabel("[ms]");
    ui->grafico->xAxis->setLabelColor(Qt::blue);
    ui->grafico->yAxis->setLabel("[V]");
    ui->grafico->yAxis->setLabelColor(Qt::blue);

    //Carga de valores anteriores guardados en memoria

    ui->grafico->addGraph();
    ui->grafico->graph(0)->setPen(QPen(QColor(255,0,0),3));
    ui->grafico->yAxis->ticker()->setTickCount(10);

    configuracion_act=configuracion_act.leer_mem();
    acomodar_limx();
    acomodar_limy();
    if(configuracion_act.onda != 3) {
        mod_arb(false);
        actualizar();
        ui->pBVpp->setEnabled(true);
        opcVppOffset->setEnabled(true);
    }
    else {
        mod_arb(true);
        actualizar();
        ui->pBVpp->setEnabled(false);
        opcVppOffset->setEnabled(false);
    }

//    ui->grafico->xAxis->setRange(0,20);
//    ui->grafico->yAxis->setRange(-1.3,1.3);
//    ui->grafico->addGraph();
//    ui->grafico->graph(0)->setData(x,y);
//    ui->grafico->graph(0)->setPen(QPen(QColor(255,0,0),3));
//    ui->grafico->replot();

//    ui->pBVpp->setText("Vpp = 2 V\nOffset = 0 V");
//    ui->pBfreq->setText("Freq = 50 Hz");
//    ui->pBdivX->setText("10 ms/div");
//    ui->pBdivY->setText("0.5 V/div");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::actualizar()
{
    float lim_sup1,lim_sup2;
    double c1;
    int c2;
    QString qtemp,qtemp2;
    configuracion_act.escribir_mem(&configuracion_act);

    switch (configuracion_act.onda) {
    case 0: {
        ui->pBond->setIcon(QIcon(":/seno.png"));
        break;
    }
    case 1: {
        ui->pBond->setIcon(QIcon(":/cuadrada.png"));
        break;
    }
    case 2: {
        ui->pBond->setIcon(QIcon(":/triangular.png"));
        break;
    }
    case 3: {
        ui->pBond->setIcon(QIcon(":/arb.png"));
        break;
    }
    }

    lim_sup1=1000/configuracion_act.freq;
    lim_tiempo=lim_sup1;
    ui->grafico->xAxis->setRange(0,limx);
    c2=configuracion_act.freq_mult+3;

    qtemp.clear();
    qtemp+="Freq = ";
    qtemp+=QString::number(configuracion_act.freq);

    qtemp2.clear();

    qtemp2+=QString::number(limx);

    switch (c2) {
    case 3: {
        ui->grafico->xAxis->setLabel("[ms]");
        qtemp+=" Hz";
        qtemp2+=" ms";
        break;
    }
    case 6: {
        ui->grafico->xAxis->setLabel("[µs]");
        qtemp+=" KHz";
        qtemp2+=" µs";
        break;
    }
    case 9: {
        ui->grafico->xAxis->setLabel("[ns]");
        qtemp+=" MHz";
        qtemp2+=" ns";
        break;
    }
    case 12: {
        ui->grafico->xAxis->setLabel("[ps]");
        qtemp+=" GHz";
        qtemp2+=" ps";
        break;
    }
    }

    ui->pBdivX->setText(qtemp2);
    ui->pBfreq->setText(qtemp);

    qtemp.clear();
    qtemp+="Amp = ";
    qtemp+=QString::number((configuracion_act.vpp)/2);
    qtemp+=" V\nOffset = ";
    qtemp+=QString::number(configuracion_act.offset);
    qtemp+=" V";
    ui->pBVpp->setText(qtemp);

    lim_sup2=limy;

    switch (configuracion_act.onda) {
    case 0: {
        ui->grafico->yAxis->setRange(-lim_sup2,lim_sup2);

        qtemp.clear();

        if(lim_sup2==15)
            qtemp+=QString::number((lim_sup2/6));
        else
            qtemp+=QString::number((lim_sup2/5));

        qtemp+=" V/div";
        ui->pBdivY->setText(qtemp);

        QVector<double> x(c_muestras+1),y(c_muestras+1);

        for(unsigned int i=0;i<c_muestras+1;i++) {
            x[i]=limx*i/c_muestras;
            c1=2*PI*configuracion_act.freq*qPow(10,configuracion_act.freq_mult);
            y[i]=((configuracion_act.vpp)/2)*qSin(c1*(x[i])*qPow(10,-(configuracion_act.freq_mult+3)))+configuracion_act.offset;
        }

        ui->grafico->graph(0)->setData(x,y);
        break;
    }
    case 1: {
        ui->grafico->yAxis->setRange(-lim_sup2,lim_sup2);

        qtemp.clear();

        if(lim_sup2==15)
            qtemp+=QString::number((lim_sup2/6));
        else
            qtemp+=QString::number((lim_sup2/5));

        qtemp+=" V/div";
        ui->pBdivY->setText(qtemp);

        QVector<double> x(c_muestras+1),y(c_muestras+1);
        double xt;

        for(unsigned int i=0;i<c_muestras+1;i++) {
            x[i]=limx*i/c_muestras;

            xt=x[i]-lim_sup1*(int)((x[i])/lim_sup1);

            if(xt<=lim_sup1/2)
                y[i]=((configuracion_act.vpp)/2)+configuracion_act.offset;
            else
                y[i]=-((configuracion_act.vpp)/2)+configuracion_act.offset;
        }

        ui->grafico->graph(0)->setData(x,y);
        break;
    }
    case 2: {
        ui->grafico->yAxis->setRange(-lim_sup2,lim_sup2);

        qtemp.clear();

        if(lim_sup2==15)
            qtemp+=QString::number((lim_sup2/6));
        else
            qtemp+=QString::number((lim_sup2/5));

        qtemp+=" V/div";
        ui->pBdivY->setText(qtemp);

        QVector<double> x(c_muestras+1),y(c_muestras+1);
        double xt;

        for(unsigned int i=0;i<c_muestras;i++) {
            x[i]=limx*i/c_muestras;

            xt=x[i]-lim_sup1*(int)((x[i])/lim_sup1);

            if(xt<=lim_sup1/4)
                y[i]=(configuracion_act.vpp*2)*xt*qPow(10,-(configuracion_act.freq_mult+3))*configuracion_act.freq*qPow(10,configuracion_act.freq_mult)+configuracion_act.offset;
            else if (xt<=lim_sup1*3/4)
                y[i]=-(configuracion_act.vpp*2)*xt*qPow(10,-(configuracion_act.freq_mult+3))*configuracion_act.freq*qPow(10,configuracion_act.freq_mult)+configuracion_act.vpp+configuracion_act.offset;
            else
                y[i]=(configuracion_act.vpp*2)*xt*qPow(10,-(configuracion_act.freq_mult+3))*configuracion_act.freq*qPow(10,configuracion_act.freq_mult)-2*configuracion_act.vpp+configuracion_act.offset;

        }

        ui->grafico->graph(0)->setData(x,y);
        break;
    }
    case 3: {
        QVector<double> xv,yv;

        ui->grafico->yAxis->setRange(-limy,limy);

        qtemp.clear();

        if(lim_sup2==15)
            qtemp+=QString::number((lim_sup2/6));
        else
            qtemp+=QString::number((lim_sup2/5));

        qtemp+=" V/div";
        ui->pBdivY->setText(qtemp);

        configuracion_act.valores_arb_x.clear();

        for(int j=0;j<c_muestras_arb+1;j++) {
            configuracion_act.valores_arb_x<<j*(lim_tiempo/c_muestras_arb);
        }

        for(int i=0;limx>=lim_tiempo*i;i++) {
            for(int j=0;j<configuracion_act.valores_arb_x.size();j++) {
                xv<<configuracion_act.valores_arb_x[j]+lim_tiempo*i;
                yv<<configuracion_act.valores_arb[j];
            }
        }

        //ui->grafico->graph(0)->setData(configuracion_act.valores_arb_x,configuracion_act.valores_arb);
        ui->grafico->graph(0)->setData(xv,yv);

        break;
    }
    }

    ui->grafico->replot();
}

void MainWindow::leer_valores(int sel)
{
    switch (sel) {
    case 0: {
        if(opcVppOffset->isVisible() == true){
            configuracion_act.vpp=(float)(opcVppOffset->leer_valor(0)/10);
            configuracion_act.offset=(float)(opcVppOffset->leer_valor(1)/10);

            acomodar_limx();
            acomodar_limy();
            actualizar();
        }
        break;
    }
    case 1: {
        if(opcFreq->isVisible() == true) {
            configuracion_act.freq=opcFreq->leer_valor(0);
            configuracion_act.freq_mult=opcFreq->leer_mult();

            actualizar();
        }
        break;
    }
    case 2: {
        if(opcdivx->isVisible() == true) {
            limx=(float)((opcdivx->leer_valor(0))*5);

            actualizar();
        }
        break;
    }
    case 10: {
        if(sOndas->isVisible() == true) {
            configuracion_act.onda=sOndas->tipo_onda;

            if(configuracion_act.onda!=3) {
                mod_arb(false);
                actualizar();
                opcVppOffset->setEnabled(true);
                ui->pBVpp->setEnabled(true);
            }
            else {
                mod_arb(true);
                actualizar();
                opcVppOffset->setEnabled(false);
                ui->pBVpp->setEnabled(false);
            }

            sOndas->hide();
        }
        break;
    }
    }
}

void MainWindow::enfocar()
{
    opcVppOffset->activateWindow();
    opcFreq->activateWindow();
    opcdivx->activateWindow();
    opcdivy->activateWindow();
    sOndas->activateWindow();
}

void MainWindow::tyv_valores()
{
    QVector<double> xe,ye;

    switch (configuracion_act.onda) {
    case 0: {

    }
    }
}

void MainWindow::restablecer_arb()
{
    if(configuracion_act.onda==3) {
        mod_arb(true);
        actualizar();
    }
}

void MainWindow::acomodar_limx()
{
    limx=1000/(configuracion_act.freq);
    lim_tiempo=limx;

    for(int i=0;i*5<1000;i++) {
        if(limx <= i*5) {
            limx = i*5;
            break;
            }
    }
}

void MainWindow::acomodar_limy()
{
    limy=(((configuracion_act.vpp)/2)+qAbs(configuracion_act.offset))*1.5;

    for(int i=0;i*5<1000;i++) {
        if(limy <= i*5) {
            limy = i*5;
            break;
            }
    }
}

void MainWindow::mod_arb(bool estado)
{
    if(estado == true) {
        ui->grafico->setInteractions(QCP::iSelectItems | QCP::iMultiSelect);

        configuracion_act.valores_arb.clear();
        configuracion_act.valores_arb_x.clear();

        for(int j=0;j<c_muestras_arb+1;j++) {
            configuracion_act.valores_arb_x<<j*(lim_tiempo/c_muestras_arb);
            configuracion_act.valores_arb<<0;
        }

        configuracion_act.escribir_mem(&configuracion_act);
    }
    else {
        ui->grafico->setInteraction(QCP::iSelectOther);
    }
}

float MainWindow::valory(float valorx)
{
    int i;
    float res;
    double yi,ymi,xi,xmi;

    for(i=0;ui->grafico->graph(0)->dataMainKey(i) < valorx;i++);

    yi=ui->grafico->graph(0)->dataMainValue(i);
    ymi=ui->grafico->graph(0)->dataMainValue(i-1);
    xi=ui->grafico->graph(0)->dataMainKey(i);
    xmi=ui->grafico->graph(0)->dataMainKey(i-1);

    if(yi > ymi)
        res = ymi + (yi - ymi) * (valorx - (float)xmi)/(xi-xmi);

    else
        res = ymi - (ymi - yi) * (valorx - (float)xmi)/(xi-xmi);

    return res;
}

str_config str_config::leer_mem()
{
    config cfg_def(2,0,50,0,0);
    QFile arch_conf("config.cfg");

    if(!arch_conf.open(QIODevice::ReadOnly | QIODevice::Text))
        return cfg_def;

    QString lect;
    QTextStream in(&arch_conf);

    lect=in.readLine();
    cfg_def.vpp=lect.toFloat();

    lect=in.readLine();
    cfg_def.offset=lect.toFloat();

    lect=in.readLine();
    cfg_def.freq=lect.toFloat();

    lect=in.readLine();
    cfg_def.freq_mult=lect.toInt();

    lect=in.readLine();
    cfg_def.onda=lect.toInt();

//    if(cfg_def.onda == 3) {
//        cfg_def.valores_arb.clear();
//        while(!in.atEnd()) {
//            lect=in.readLine();

//            if(!in.atEnd())
//                cfg_def.valores_arb.append(lect.toDouble());
//        }
//    }

    arch_conf.close();

    return cfg_def;
}

void str_config::escribir_mem(str_config *actual)
{
    QFile arch_conf("config.cfg");
    arch_conf.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&arch_conf);
    out<<actual->vpp<<endl<<actual->offset<<endl<<actual->freq<<endl<<actual->freq_mult<<endl<<actual->onda<<endl;

//    for(int i=0;i<actual->valores_arb.size();i++) {
//        out<<actual->valores_arb[i]<<endl;
//    }

    arch_conf.close();
}

void MainWindow::on_pBVpp_clicked()
{
    if(opcVppOffset->isVisible() == false){
        opcVppOffset->show();
        opcVppOffset->cargar_valor((int)((configuracion_act.vpp)*10),(int)((configuracion_act.offset)*10));
    }
    else{
        opcVppOffset->hide();
    }

}

void MainWindow::on_pBfreq_clicked()
{
    char el;

    switch (configuracion_act.freq_mult) {
    case 0: {
        el='H';
        break;
    }
    case 3: {
        el='K';
        break;
    }
    case 6: {
        el='M';
        break;
    }
    case 9: {
        el='G';
        break;
    }
    }

    if(opcFreq->isVisible() == false) {
        opcFreq->show();
        opcFreq->cargar_valor((int)(configuracion_act.freq),el);
    }
    else {
        opcFreq->hide();
    }
}

void MainWindow::on_pBond_clicked()
{
    if(sOndas->isVisible() == false) {
        sOndas->show();
        sOndas->tipo_onda=configuracion_act.onda;
    }
    else {
        sOndas->hide();
    }
}

void MainWindow::on_MainWindow_destroyed()
{

}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    enfocar();
}

void MainWindow::on_pBdivX_clicked()
{
    if(opcdivx->isVisible() == false) {
        opcdivx->show();
        opcdivx->cargar_valor((int)(limx/5));
    }
    else {
        opcdivx->hide();
    }
}

void MainWindow::mousePress(QMouseEvent* evento)
{
  if(configuracion_act.onda == 3)
    emit llamar_act(1,evento);
}

void MainWindow::mousMove(QMouseEvent* conecs)
{
    if(configuracion_act.onda == 3)
        emit llamar_act(2,conecs);
}

void MainWindow::mousRelease(QMouseEvent* conecs)
{
    if(configuracion_act.onda == 3)
        emit llamar_act(3,conecs);
}

void MainWindow::mover_graph(int estado,QMouseEvent* evento)
{ //estado , 0: nada, 1: click, 2:moviendo con click, 3:solto

    if(estado==1) {
        if(act_estado==0) {
            act_estado=1;
            double xg=0,yg=0;
            int i=0;

            ui->grafico->graph(0)->pixelsToCoords(evento->x(),evento->y(),xg,yg);

            if(xg<lim_tiempo) {
                for(i=0;configuracion_act.valores_arb_x[i]<=xg;i++);

                configuracion_act.valores_arb[i]=yg;

                actualizar();
            }
        }
    }

    if(estado==2) {
        if(act_estado==1 || act_estado==2) {
            act_estado=1;
            double xg=0,yg=0;
            int i=0;

            ui->grafico->graph(0)->pixelsToCoords(evento->x(),evento->y(),xg,yg);

            if(xg<lim_tiempo) {
                for(i=0;configuracion_act.valores_arb_x[i]<=xg;i++);

                configuracion_act.valores_arb[i]=yg;

                actualizar();
            }
        }
    }

    if(estado==3) {
        if(act_estado==1 || act_estado==2) {
            act_estado=0;
        }
    }
}

void MainWindow::on_pBopc_clicked()
{
    if(mOpc->isVisible() == false)
        mOpc->show();
    else
        mOpc->hide();

}
