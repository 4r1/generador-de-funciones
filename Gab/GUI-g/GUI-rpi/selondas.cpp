#include "selondas.h"
#include "ui_selondas.h"

selOndas::selOndas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::selOndas)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowTitleHint);
}

selOndas::~selOndas()
{
    delete ui;
}

void selOndas::on_pBond_3_clicked()
{
    tipo_onda=0;

    emit valor_cambiado(10);
}

void selOndas::on_pBond_4_clicked()
{
    tipo_onda=1;

    emit valor_cambiado(10);
}


void selOndas::on_pBond_2_clicked()
{
    tipo_onda=2;

    emit valor_cambiado(10);
}

void selOndas::on_pBond_5_clicked()
{
    tipo_onda=3;

    emit valor_cambiado(10);
}
