/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFrame *line;
    QFrame *line_4;
    QFrame *line_5;
    QFrame *line_6;
    QFrame *line_7;
    QFrame *line_8;
    QPushButton *pBVpp;
    QPushButton *pBfreq;
    QPushButton *pBond;
    QPushButton *pBdivX;
    QPushButton *pBdivY;
    QPushButton *pBopc;
    QCustomPlot *grafico;
    QLabel *label_espera;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(480, 320);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(480, 320));
        MainWindow->setMaximumSize(QSize(480, 320));
        MainWindow->setMouseTracking(true);
        MainWindow->setToolTipDuration(-1);
        MainWindow->setStyleSheet(QStringLiteral("background-color: black"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setAutoFillBackground(false);
        centralWidget->setStyleSheet(QStringLiteral("background: black"));
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(0, 287, 480, 4));
        line->setStyleSheet(QStringLiteral("color: white"));
        line->setFrameShadow(QFrame::Plain);
        line->setLineWidth(2);
        line->setFrameShape(QFrame::HLine);
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(422, 288, 3, 32));
        line_4->setStyleSheet(QStringLiteral("color: white"));
        line_4->setFrameShadow(QFrame::Plain);
        line_4->setLineWidth(2);
        line_4->setFrameShape(QFrame::VLine);
        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(350, 288, 3, 32));
        line_5->setStyleSheet(QStringLiteral("color: white"));
        line_5->setFrameShadow(QFrame::Plain);
        line_5->setLineWidth(2);
        line_5->setFrameShape(QFrame::VLine);
        line_6 = new QFrame(centralWidget);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setGeometry(QRect(278, 288, 3, 32));
        line_6->setStyleSheet(QStringLiteral("color: white"));
        line_6->setFrameShadow(QFrame::Plain);
        line_6->setLineWidth(2);
        line_6->setFrameShape(QFrame::VLine);
        line_7 = new QFrame(centralWidget);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setGeometry(QRect(192, 288, 3, 32));
        line_7->setStyleSheet(QStringLiteral("color: white"));
        line_7->setFrameShadow(QFrame::Plain);
        line_7->setLineWidth(2);
        line_7->setFrameShape(QFrame::VLine);
        line_8 = new QFrame(centralWidget);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setGeometry(QRect(96, 288, 3, 32));
        line_8->setStyleSheet(QStringLiteral("color: white"));
        line_8->setFrameShadow(QFrame::Plain);
        line_8->setLineWidth(2);
        line_8->setFrameShape(QFrame::VLine);
        pBVpp = new QPushButton(centralWidget);
        pBVpp->setObjectName(QStringLiteral("pBVpp"));
        pBVpp->setGeometry(QRect(3, 290, 88, 27));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        QBrush brush1(QColor(255, 255, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        QBrush brush2(QColor(190, 190, 190, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        pBVpp->setPalette(palette);
        QFont font;
        font.setPointSize(9);
        pBVpp->setFont(font);
        pBVpp->setAutoFillBackground(false);
        pBVpp->setFlat(true);
        pBfreq = new QPushButton(centralWidget);
        pBfreq->setObjectName(QStringLiteral("pBfreq"));
        pBfreq->setGeometry(QRect(100, 290, 88, 27));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush);
        QBrush brush3(QColor(238, 238, 236, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush3);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush);
        pBfreq->setPalette(palette1);
        pBfreq->setFont(font);
        pBfreq->setAutoFillBackground(false);
        pBfreq->setFlat(true);
        pBond = new QPushButton(centralWidget);
        pBond->setObjectName(QStringLiteral("pBond"));
        pBond->setGeometry(QRect(200, 290, 73, 27));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush);
        palette2.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush3);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush);
        pBond->setPalette(palette2);
        pBond->setAutoFillBackground(false);
        QIcon icon;
        icon.addFile(QStringLiteral(":/seno.png"), QSize(), QIcon::Normal, QIcon::Off);
        pBond->setIcon(icon);
        pBond->setIconSize(QSize(56, 40));
        pBond->setFlat(true);
        pBdivX = new QPushButton(centralWidget);
        pBdivX->setObjectName(QStringLiteral("pBdivX"));
        pBdivX->setEnabled(false);
        pBdivX->setGeometry(QRect(282, 290, 64, 27));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush);
        palette3.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush3);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush);
        pBdivX->setPalette(palette3);
        pBdivX->setFont(font);
        pBdivX->setAutoFillBackground(false);
        pBdivX->setFlat(true);
        pBdivY = new QPushButton(centralWidget);
        pBdivY->setObjectName(QStringLiteral("pBdivY"));
        pBdivY->setGeometry(QRect(355, 290, 64, 27));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush);
        palette4.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush3);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush);
        pBdivY->setPalette(palette4);
        pBdivY->setFont(font);
        pBdivY->setAutoFillBackground(false);
        pBdivY->setFlat(true);
        pBopc = new QPushButton(centralWidget);
        pBopc->setObjectName(QStringLiteral("pBopc"));
        pBopc->setGeometry(QRect(430, 293, 44, 25));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/opc.png"), QSize(), QIcon::Normal, QIcon::Off);
        pBopc->setIcon(icon1);
        pBopc->setIconSize(QSize(30, 30));
        pBopc->setFlat(true);
        grafico = new QCustomPlot(centralWidget);
        grafico->setObjectName(QStringLiteral("grafico"));
        grafico->setGeometry(QRect(1, 1, 478, 286));
        grafico->setFocusPolicy(Qt::NoFocus);
        grafico->setAutoFillBackground(false);
        grafico->setStyleSheet(QStringLiteral(""));
        label_espera = new QLabel(grafico);
        label_espera->setObjectName(QStringLiteral("label_espera"));
        label_espera->setGeometry(QRect(1, 2, 477, 281));
        QPalette palette5;
        QBrush brush4(QColor(0, 0, 0, 150));
        brush4.setStyle(Qt::SolidPattern);
        palette5.setBrush(QPalette::Active, QPalette::Button, brush4);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush4);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush4);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush4);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush4);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush4);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush4);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush4);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush4);
        label_espera->setPalette(palette5);
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setUnderline(false);
        font1.setWeight(75);
        label_espera->setFont(font1);
        label_espera->setStyleSheet(QStringLiteral("background:rgba(0,0,0,150)"));
        label_espera->setFrameShape(QFrame::Box);
        label_espera->setTextFormat(Qt::PlainText);
        label_espera->setScaledContents(false);
        label_espera->setAlignment(Qt::AlignCenter);
        label_4 = new QLabel(grafico);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(0, 240, 45, 45));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/logo.png")));
        label_4->setScaledContents(true);
        label_5 = new QLabel(grafico);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(44, 264, 141, 21));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/nombre_logo.png")));
        label_5->setScaledContents(true);
        label_6 = new QLabel(grafico);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(4, 4, 31, 37));
        label_6->setTextFormat(Qt::PlainText);
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/logoutn.png")));
        label_6->setScaledContents(true);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(-6, 290, 491, 32));
        label->setAutoFillBackground(false);
        label->setStyleSheet(QStringLiteral("background: rgb(70,0,0)"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(192, 288, 87, 34));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(0, 0, 480, 320));
        label_3->setAutoFillBackground(false);
        label_3->setStyleSheet(QStringLiteral("background: black"));
        MainWindow->setCentralWidget(centralWidget);
        label_3->raise();
        label->raise();
        label_2->raise();
        line->raise();
        line_4->raise();
        line_5->raise();
        line_6->raise();
        line_7->raise();
        line_8->raise();
        pBVpp->raise();
        pBfreq->raise();
        pBond->raise();
        pBdivX->raise();
        pBdivY->raise();
        pBopc->raise();
        grafico->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Generador", Q_NULLPTR));
        pBVpp->setText(QApplication::translate("MainWindow", "Vpp = \n"
"Offset = ", Q_NULLPTR));
        pBfreq->setText(QApplication::translate("MainWindow", "Freq = ", Q_NULLPTR));
        pBond->setText(QString());
        pBdivX->setText(QApplication::translate("MainWindow", "10 ms/div", Q_NULLPTR));
        pBdivY->setText(QApplication::translate("MainWindow", "5 V/div", Q_NULLPTR));
        pBopc->setText(QString());
        label_espera->setText(QString());
        label_4->setText(QString());
        label_5->setText(QString());
        label_6->setText(QString());
        label->setText(QString());
        label_2->setText(QString());
        label_3->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
