
#include <stdio.h>

int main(void){
	int c;
	FILE *entrada;
	FILE *salida;
	entrada = fopen("valores_salida", "r");
	salida = fopen("programa.s", "a");
	if (entrada && salida) {
    		while ((c = getc(entrada)) != EOF){
			for (int i=0; i<8; i++){
				if(c=='0'){
					switch(i){
						case 7: fputs("\tstr\t r6, [r3, #40]\n", salida); break;
						case 6: fputs("\tstr\t r5, [r3, #40]\n", salida); break;
						case 5: fputs("\tstr\t r4, [r3, #40]\n", salida); break;
						case 4: fputs("\tstr\t lr, [r3, #40]\n", salida); break;
						case 3: fputs("\tstr\t ip, [r3, #40]\n", salida); break;
						case 2: fputs("\tstr\t r0, [r3, #40]\n", salida); break;
						case 1: fputs("\tstr\t r1, [r3, #40]\n", salida); break;
						case 0: fputs("\tstr\t r2, [r3, #40]\n", salida); break;
					}
				}
				if(c=='1'){
					switch(i){
						case 7: fputs("\tstr\t r6, [r3, #28]\n", salida); break;
						case 6: fputs("\tstr\t r5, [r3, #28]\n", salida); break;
						case 5: fputs("\tstr\t r4, [r3, #28]\n", salida); break;
						case 4: fputs("\tstr\t lr, [r3, #28]\n", salida); break;
						case 3: fputs("\tstr\t ip, [r3, #28]\n", salida); break;
						case 2: fputs("\tstr\t r0, [r3, #28]\n", salida); break;
						case 1: fputs("\tstr\t r1, [r3, #28]\n", salida); break;
						case 0: fputs("\tstr\t r2, [r3, #28]\n", salida); break;
					}
				}
				c = getc(entrada);
			}
        		fputc('\n', salida);
		}
    		fclose(entrada);
    		fclose(salida);
	}else{
		return 1;
	}
	return 0;
}

