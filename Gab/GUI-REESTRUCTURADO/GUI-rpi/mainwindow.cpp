#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Inicializacion de variables
    Vmax=-100;
    Vmin=100;
    c_muestras=1000;
    c_muestras_arb=80;
    punt_e=0;
    act_estado=0;
    actualizandob=false;

    opcVppOffset=new opciones(this);
    opcFreq=new opciones(this);
    opcdivx=new opciones(this);
    opcdivy=new opciones(this);
    sOndas=new selOndas(this);
    mOpc=new menuOpc(this);
    escritura_valores=new QFile("valores_salida",this);
    escritura_valores2=new QFile("caracteristicas",this);
    lectura_estado_act=new QFile("estado", this);
    puntitos=new QTimer(this);
    puntitos->setInterval(450);
    leer_estado= new QTimer(this);
    leer_estado->setInterval(200);

    lectura_estado_act->open(QIODevice::ReadWrite);

    opcVppOffset->hide();
    opcFreq->hide();
    opcdivx->hide();
    opcdivy->hide();
    sOndas->hide();
    mOpc->hide();
    ui->label_espera->hide();
    //ui->label_6->setVisible(false);
    ui->label_6->setWindowOpacity(150);

    connect(opcVppOffset,SIGNAL(valor_cambiado(int)),this,SLOT(leer_valores(int)));
    connect(opcFreq,SIGNAL(valor_cambiado(int)),this,SLOT(leer_valores(int)));
    connect(opcdivy,SIGNAL(valor_cambiado(int)),this,SLOT(leer_valores(int)));
    connect(sOndas,SIGNAL(valor_cambiado(int)), this, SLOT(leer_valores(int)));
    connect(ui->grafico, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress(QMouseEvent*)));
    connect(ui->grafico, SIGNAL(mouseMove(QMouseEvent*)),this, SLOT(mousMove(QMouseEvent*)));
    connect(ui->grafico, SIGNAL(mouseRelease(QMouseEvent*)),this, SLOT(mousRelease(QMouseEvent*)));
    connect(this, SIGNAL(llamar_act(int,QMouseEvent*)),this, SLOT(mover_graph(int,QMouseEvent*)));
    connect(mOpc,SIGNAL(enviar_valores()),this,SLOT(tyv_valores()));
    connect(mOpc,SIGNAL(restablecer_arb()),this,SLOT(restablecer_arb()));
    connect(puntitos,SIGNAL(timeout()),this,SLOT(punt()));
    connect(leer_estado,SIGNAL(timeout()),this,SLOT(leer_estado_f()));

    opcVppOffset->inic(0);
    opcVppOffset->move(0,86);
    opcFreq->inic(1);
    opcFreq->move(128,86);
    opcdivx->inic(2);
    opcdivx->move(280,86);
    opcdivy->inic(3);
    opcdivy->move(128,86);
    sOndas->move(192,48);
    mOpc->move(351,154);

    ui->grafico->xAxis->setBasePen(QPen(Qt::blue,1));
    ui->grafico->yAxis->setBasePen(QPen(Qt::blue,1));
    ui->grafico->xAxis->setTickPen(QPen(Qt::blue, 1));
    ui->grafico->yAxis->setTickPen(QPen(Qt::blue, 1));
    ui->grafico->xAxis->setSubTickPen(QPen(Qt::blue, 1));
    ui->grafico->yAxis->setSubTickPen(QPen(Qt::blue, 1));
    ui->grafico->yAxis->axisRect()->setAutoMargins(QCP::msNone);
    ui->grafico->yAxis->axisRect()->setMargins(QMargins(70,15,15,45));
    ui->grafico->yAxis->setTickLabelPadding(5);
    ui->grafico->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->grafico->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->grafico->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->grafico->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->grafico->xAxis->grid()->setSubGridVisible(true);
    ui->grafico->yAxis->grid()->setSubGridVisible(true);
    ui->grafico->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->grafico->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->grafico->xAxis->setLabel("[ms]");
    ui->grafico->xAxis->setLabelColor(Qt::blue);
    ui->grafico->yAxis->setLabel("[V]");
    ui->grafico->yAxis->setLabelColor(Qt::blue);

    ui->grafico->addGraph();
    ui->grafico->graph(0)->setPen(QPen(QColor(255,0,0),3));
    ui->grafico->graph(0)->setLineStyle((QCPGraph::LineStyle)QCPGraph::lsStepLeft);
    ui->grafico->yAxis->ticker()->setTickCount(10);

    configuracion_act=configuracion_act.leer_mem();
    configuracion_act.freq=1;
    configuracion_act.freq_mult=3;
    configuracion_act.escribir_mem(&configuracion_act);

    inic_onda();

}

void MainWindow::inic_onda() {
    acomodar_limx();
    acomodar_limy();
    definir_muestras();

    if(configuracion_act.onda != 3) {
        mod_arb(false);
        actualizar();
        ui->pBVpp->setEnabled(true);
        opcVppOffset->setEnabled(true);
        ui->pBdivY->setEnabled(false);
    }
    else {
        arb_rango=10;
        mod_arb(true);
        actualizar();
        ui->pBVpp->setEnabled(false);
        opcVppOffset->setEnabled(false);
        ui->pBdivY->setEnabled(true);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::actualizar()
{
    float lim_sup1,lim_sup2;
    double c1;
    int c2;
    QString qtemp,qtemp2;
    configuracion_act.escribir_mem(&configuracion_act);

    switch (configuracion_act.onda) {
    case 0: {
        ui->pBond->setIcon(QIcon(":/seno.png"));
        break;
    }
    case 1: {
        ui->pBond->setIcon(QIcon(":/cuadrada.png"));
        break;
    }
    case 2: {
        ui->pBond->setIcon(QIcon(":/triangular.png"));
        break;
    }
    case 3: {
        ui->pBond->setIcon(QIcon(":/arb.png"));
        break;
    }
    }

    lim_sup1=lim_tiempo;

    ui->grafico->xAxis->setRange(0,limx);
    c2=configuracion_act.freq_mult+3;

    qtemp.clear();
    qtemp+="Freq = ";
    qtemp+=QString::number(configuracion_act.freq);

    qtemp2.clear();

    for(int l=0;l<5;l++) {
        if(QString::number(limx).toStdString().c_str()[l] == 0)
            break;

        qtemp2+=QString::number(limx).toStdString().c_str()[l];
    }

    switch (c2) {
    case 3: {
        ui->grafico->xAxis->setLabel("[ms]");
        qtemp+=" Hz";
        qtemp2+=" ms";
        break;
    }
    case 6: {
        ui->grafico->xAxis->setLabel("[µs]");
        qtemp+=" KHz";
        qtemp2+=" µs";
        break;
    }
    case 9: {
        ui->grafico->xAxis->setLabel("[ns]");
        qtemp+=" MHz";
        qtemp2+=" ns";
        break;
    }
    case 12: {
        ui->grafico->xAxis->setLabel("[ps]");
        qtemp+=" GHz";
        qtemp2+=" ps";
        break;
    }
    }

    ui->pBdivX->setText(qtemp2);
    ui->pBfreq->setText(qtemp);

    qtemp.clear();
    qtemp+="Amp = ";
    qtemp+=QString::number((configuracion_act.vpp)/2);
    qtemp+=" V\nOffset = ";
    qtemp+=QString::number(configuracion_act.offset);
    qtemp+=" V";
    ui->pBVpp->setText(qtemp);

    lim_sup2=limy;

    switch (configuracion_act.onda) {
    case 0: {
        ui->grafico->yAxis->setRange(-lim_sup2,lim_sup2);

        qtemp.clear();
        qtemp+=QString::number(lim_sup2);
        qtemp+=" V";
        ui->pBdivY->setText(qtemp);

        QVector<double> x(c_muestras+1),y(c_muestras+1);

        for(int i=0;i<c_muestras+1;i++) {
            x[i]=limx*i/c_muestras;
            c1=2*PI*configuracion_act.freq*qPow(10,configuracion_act.freq_mult);
            y[i]=((configuracion_act.vpp)/2)*qSin(c1*(x[i])*qPow(10,-(configuracion_act.freq_mult+3)))+configuracion_act.offset;
        }

        ui->grafico->graph(0)->setData(x,y);
        break;
    }
    case 1: {
        ui->grafico->yAxis->setRange(-lim_sup2,lim_sup2);

        qtemp.clear();
        qtemp+=QString::number(lim_sup2);
        qtemp+=" V";
        ui->pBdivY->setText(qtemp);

        QVector<double> x(c_muestras+1),y(c_muestras+1);
        double xt;

        for(int i=0;i<c_muestras+1;i++) {
            x[i]=limx*i/c_muestras;

            xt=x[i]-lim_sup1*(int)((x[i])/lim_sup1);

            if(xt<=lim_sup1/2)
                y[i]=((configuracion_act.vpp)/2)+configuracion_act.offset;
            if(xt>=lim_sup1/2)
                y[i]=-((configuracion_act.vpp)/2)+configuracion_act.offset;
        }

        ui->grafico->graph(0)->setData(x,y);
        break;
    }
    case 2: {
        ui->grafico->yAxis->setRange(-lim_sup2,lim_sup2);

        qtemp.clear();
        qtemp+=QString::number(lim_sup2);
        qtemp+=" V";
        ui->pBdivY->setText(qtemp);

        QVector<double> x(c_muestras+1),y(c_muestras+1);
        double xt;

        for(int i=0;i<c_muestras+1;i++) {
            x[i]=limx*i/c_muestras;

            xt=x[i]-lim_sup1*(int)((x[i])/lim_sup1);

            if(xt<=lim_sup1/4)
                y[i]=(configuracion_act.vpp*2)*xt*qPow(10,-(configuracion_act.freq_mult+3))*configuracion_act.freq*qPow(10,configuracion_act.freq_mult)+configuracion_act.offset;
            else if (xt<=lim_sup1*3/4)
                y[i]=-(configuracion_act.vpp*2)*xt*qPow(10,-(configuracion_act.freq_mult+3))*configuracion_act.freq*qPow(10,configuracion_act.freq_mult)+configuracion_act.vpp+configuracion_act.offset;
            else
                y[i]=(configuracion_act.vpp*2)*xt*qPow(10,-(configuracion_act.freq_mult+3))*configuracion_act.freq*qPow(10,configuracion_act.freq_mult)-2*configuracion_act.vpp+configuracion_act.offset;

        }

        ui->grafico->graph(0)->setData(x,y);
        break;
    }
    case 3: {
        QVector<double> xv,yv;

        ui->grafico->yAxis->setRange(-arb_rango,arb_rango);

        qtemp.clear();

        qtemp+=QString::number(arb_rango);

        qtemp+=" V";
        ui->pBdivY->setText(qtemp);

        configuracion_act.valores_arb_x.clear();

        for(int j=0;j<c_muestras_arb+1;j++) {
            configuracion_act.valores_arb_x<<j*(lim_tiempo/c_muestras_arb);
        }

        for(int i=0;limx>=lim_tiempo*i;i++) {
            for(int j=0;j<c_muestras_arb+1;j++) {
                if(!(i!=0 && j==0)){
                    xv<<configuracion_act.valores_arb_x[j]+lim_tiempo*i;
                    yv<<configuracion_act.valores_arb[j];
                }
            }
        }

        ui->grafico->graph(0)->setData(xv,yv);

        break;
    }
    }

    ui->grafico->replot();
}

void MainWindow::leer_valores(int sel)
{
    switch (sel) {
    case 0: {
        if(opcVppOffset->isVisible() == true){
            float ftemp;

            configuracion_act.vpp=(float)(opcVppOffset->leer_valor(0)/10);
            ftemp=opcVppOffset->leer_valor(1);

            configuracion_act.offset=ftemp/10;

            acomodar_limy();
            actualizar();
        }
        break;
    }
    case 1: {
        if(opcFreq->isVisible() == true) {
            configuracion_act.freq=opcFreq->leer_valor(0);
            configuracion_act.freq_mult=opcFreq->leer_mult();

            verificar_limite();

            acomodar_limx();
            definir_muestras();

            if(configuracion_act.onda == 3)
                mod_arb(true);

            actualizar();
        }
        break;
    }
    case 10: {
        if(sOndas->isVisible() == true) {
            configuracion_act.onda=sOndas->tipo_onda;


            inic_onda();

            sOndas->hide();
        }
        break;
    }
    }
}

void MainWindow::tyv_valores()
{
    float it;
    float ftemp1;
    unsigned int itemp2;
    std::bitset<8> *bstemp;
    float fftemp;
    double ddtemp, sum=0;
    int arb_rango_viejo=0;

    Vmax=-100;
    Vmin=100;

    if(configuracion_act.onda == 3) {
        arb_rango_viejo=arb_rango;
        arb_rango=10;

        actualizar();
    }

    actualizandob=true;
    lectura_estado_act->open(QIODevice::WriteOnly);
    lectura_estado_act->write("0");
    lectura_estado_act->close();

    float vect_temp[cantidad_muestras_total];

    for(int k=1;k<=cantidad_muestras_total;k++) {
        ddtemp=lim_tiempo/cantidad_muestras_total;
        fftemp=valory(k*ddtemp);

        if(fftemp > Vmax)
            Vmax=fftemp;
        if(fftemp < Vmin)
            Vmin=fftemp;

        vect_temp[k-1]=fftemp;
        sum+=fftemp;
    }

    it=Vmax - Vmin;
    it=qFabs(it);

    escritura_valores2->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream escout2(escritura_valores2);

    escritura_valores->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream escout(escritura_valores);

    for(int i=0;i<cantidad_muestras_total;i++) {
        ftemp1=qFabs(vect_temp[i] - Vmin);
        ftemp1=ftemp1/it;

        if(configuracion_act.onda == 1 ) {
            if(ftemp1 > 0)
                ftemp1=1;
            else
                ftemp1=0;
        }

        itemp2=255*ftemp1;

        bstemp=new std::bitset<8>(itemp2);

        //escout<<QString::number(vect_temp[i])<<endl;
        escout<< QString::fromStdString(bstemp->to_string())<<endl;
        delete bstemp;
    }

    QString qqtemp1, qqtemp2;

    if(configuracion_act.onda == 3) {
        ddtemp=cantidad_muestras_total;
        qqtemp1=QString::number(sum/ddtemp);
    }
    else {
        qqtemp1=QString::number(configuracion_act.offset);
        sum=configuracion_act.offset;
    }

    bool ap=false;

    if(sum >= 0) {
        qqtemp2+='+';

        for(int l=0;l<4;l++) {
            if(qqtemp1.toStdString().c_str()[l] == 0)
                ap=true;

            if(ap)
            {
                if(l == 1)
                    qqtemp2 += '.';
                else
                    qqtemp2+='0';
            }
            else
                qqtemp2+=qqtemp1.toStdString().c_str()[l];
        }
    }
    else {
        for(int l=0;l<5;l++) {
            if(qqtemp1.toStdString().c_str()[l] == 0)
                ap=true;

            if(ap)
            {
                if(l == 2)
                    qqtemp2 += '.';
                else
                    qqtemp2+='0';
            }
            else
                qqtemp2+=qqtemp1.toStdString().c_str()[l];
        }
    }

    escout2<<qqtemp2;

    escritura_valores2->close();
    escritura_valores->close();

    if(configuracion_act.onda == 3)
        arb_rango=arb_rango_viejo;

    system("./compilador.sh &");

    //ui->label_6->setVisible(false);
    mOpc->hide();
}

void MainWindow::leer_estado_f(){
    char cltmp='1';

    if(!lectura_estado_act->isOpen()) {
        lectura_estado_act->open(QIODevice::ReadOnly);

       if (lectura_estado_act->isOpen()) {
            lectura_estado_act->getChar(&cltmp);
            lectura_estado_act->close();
        }
    }

    if(cltmp == '0' || (actualizandob && actualizando == 0) ) {
        actualizando=1;

        if(!(ui->label_espera->isVisible()))
            ui->label_espera->show();

        if(!(puntitos->isActive()))
            puntitos->start();

        if(ui->grafico->isEnabled())
            ui->grafico->setEnabled(false);

        if(ui->pBVpp->isEnabled())
            ui->pBVpp->setEnabled(false);

        if(ui->pBdivY->isEnabled())
            ui->pBdivY->setEnabled(false);

        if(ui->pBfreq->isEnabled())
            ui->pBfreq->setEnabled(false);

        if(ui->pBond->isEnabled())
            ui->pBond->setEnabled(false);

        if(ui->pBopc->isEnabled())
            ui->pBopc->setEnabled(false);
    }
    else{
        if(ui->label_espera->isVisible())
            ui->label_espera->hide();

        if(puntitos->isActive())
            puntitos->stop();

        if(!(ui->grafico->isEnabled()))
            ui->grafico->setEnabled(true);

        if(!(ui->pBVpp->isEnabled()) && configuracion_act.onda != 3)
            ui->pBVpp->setEnabled(true);

        if(!(ui->pBdivY->isEnabled()) && configuracion_act.onda == 3)
            ui->pBdivY->setEnabled(true);

        if(!(ui->pBfreq->isEnabled()))
            ui->pBfreq->setEnabled(true);

        if(!(ui->pBond->isEnabled()))
            ui->pBond->setEnabled(true);

        if(!(ui->pBopc->isEnabled()))
            ui->pBopc->setEnabled(true);

        if(actualizando == 1){
            leer_estado->stop();
            actualizando=0;
            actualizandob=false;
        }
    }
}

void MainWindow::restablecer_arb()
{
    if(configuracion_act.onda==3) {
        arb_rango=10;
        definir_muestras();
        mod_arb(true);
        actualizar();
    }

    escritura_valores2->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream escout2(escritura_valores2);

    escritura_valores->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream escout(escritura_valores);

    escout<<"00000000"<<endl;
    escout2<<"+0.00";

    escritura_valores2->close();
    escritura_valores->close();

    system("./compilador.sh &");

    //ui->label_6->setVisible(false);
    leer_estado->stop();
    mOpc->hide();
}

void MainWindow::acomodar_limx()
{
    limx=1000/(configuracion_act.freq);
    lim_tiempo=limx;

    limx=1.1*limx;
}

void MainWindow::acomodar_limy()
{
    limy=(((configuracion_act.vpp)/2)+qAbs(configuracion_act.offset))*1.5;
}

void MainWindow::mod_arb(bool estado)
{
    if(estado == true) {
        ui->grafico->setInteractions(QCP::iSelectItems | QCP::iMultiSelect);

        configuracion_act.valores_arb.clear();
        configuracion_act.valores_arb_x.clear();

        for(int j=0;j<c_muestras_arb+1;j++) {
            configuracion_act.valores_arb_x<<j*(lim_tiempo/c_muestras_arb);
            configuracion_act.valores_arb<<0;
        }

        arb_rango=10;

        configuracion_act.escribir_mem(&configuracion_act);
    }
    else {
        ui->grafico->setInteraction(QCP::iSelectOther);
    }
}

float MainWindow::valory(double valorx)
{
    int i;
    float res;
    double yi,ymi,xi,xmi;

    for(i=0;ui->grafico->graph(0)->dataMainKey(i) < valorx;i++);

    yi=ui->grafico->graph(0)->dataMainValue(i);

    if(i!=0)
        ymi=ui->grafico->graph(0)->dataMainValue(i-1);
    else {
        ymi=ui->grafico->graph(0)->dataMainValue(c_muestras);
    }

    xi=ui->grafico->graph(0)->dataMainKey(i);

    if(i!=0)
        xmi=ui->grafico->graph(0)->dataMainKey(i-1);
    else {
        xmi=ui->grafico->graph(0)->dataMainKey(c_muestras);
    }

    if(yi > ymi)
        res = ymi + (yi - ymi) * (valorx - (float)xmi)/(xi-xmi);

    else
        res = ymi - (ymi - yi) * (valorx - (float)xmi)/(xi-xmi);

    return res;
}

void MainWindow::definir_muestras()
{
    double prop,prop1;

    prop=c_muestras_1ms;

    switch (configuracion_act.freq_mult) {
    case 3: {
        prop=prop/1000;
        break;
    }
    case 6: {
        prop=prop/1000000;
        break;
    }
    case 9: {
        prop=prop/1000000000;
        break;
    }
    }

    prop1=lim_tiempo;
    prop=prop1*prop;
    cantidad_muestras_total=prop;

    if(cantidad_muestras_total >= 5 && cantidad_muestras_total <= 100) {
        c_muestras=cantidad_muestras_total;
        c_muestras_arb=cantidad_muestras_total;
    }
    else if (cantidad_muestras_total < 5){
        c_muestras=5;
        c_muestras_arb=5;
    }
    else if (cantidad_muestras_total > 100) {
        c_muestras=100;
        c_muestras_arb=100;
    }
}

void MainWindow::verificar_limite()
{
    int mm, max;

    switch(configuracion_act.onda) {
    case 0: {
        mm=limitem_0;
        max=limites_0;
        break;
    }
    case 1: {
        mm=limitem_1;
        max=limites_1;
        break;
    }
    case 2: {
        mm=limitem_2;
        max=limites_2;
        break;
    }
    case 3: {
        mm=limitem_3;
        max=limites_3;
        break;
    }
    }

    if((configuracion_act.freq_mult > mm) || (configuracion_act.freq_mult == mm && configuracion_act.freq > max)) {
        char el;

        switch (mm) {
        case 0: {
            el='H';
            break;
        }
        case 3: {
            el='K';
            break;
        }
        case 6: {
            el='M';
            break;
        }
        case 9: {
            el='G';
            break;
        }
        }

        opcFreq->cargar_valor(max,el);

        configuracion_act.freq=opcFreq->leer_valor(0);
        configuracion_act.freq_mult=opcFreq->leer_mult();

        acomodar_limx();
        definir_muestras();

        if(configuracion_act.onda == 3)
            mod_arb(true);

        actualizar();
    }
}

str_config str_config::leer_mem()
{
    config cfg_def(2,0,50,0,0);
    QFile arch_conf("config.cfg");

    if(!arch_conf.open(QIODevice::ReadOnly | QIODevice::Text))
        return cfg_def;

    QString lect;
    QTextStream in(&arch_conf);

    lect=in.readLine();
    cfg_def.vpp=lect.toFloat();

    lect=in.readLine();
    cfg_def.offset=lect.toFloat();

    lect=in.readLine();
    cfg_def.freq=lect.toFloat();

    lect=in.readLine();
    cfg_def.freq_mult=lect.toInt();

    lect=in.readLine();
    cfg_def.onda=lect.toInt();

    arch_conf.close();

    return cfg_def;
}

void str_config::escribir_mem(str_config *actual)
{
    QFile arch_conf("config.cfg");
    arch_conf.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&arch_conf);
    out<<actual->vpp<<endl<<actual->offset<<endl<<actual->freq<<endl<<actual->freq_mult<<endl<<actual->onda<<endl;

    arch_conf.close();
}

void MainWindow::on_pBVpp_clicked()
{
    if(opcVppOffset->isVisible() == false){
        opcVppOffset->show();
        opcVppOffset->cargar_valor((int)((configuracion_act.vpp)*10),(int)((configuracion_act.offset)*10));
    }
    else{
        opcVppOffset->hide();
    }
}

void MainWindow::on_pBfreq_clicked()
{
    char el='e';

    switch (configuracion_act.freq_mult) {
    case 0: {
        el='H';
        break;
    }
    case 3: {
        el='K';
        break;
    }
    case 6: {
        el='M';
        break;
    }
    case 9: {
        el='G';
        break;
    }
    }

    if(opcFreq->isVisible() == false) {
        opcFreq->cargar_valor((int)(configuracion_act.freq),el);
        opcFreq->show();
    }
    else {
        opcFreq->hide();
    }
}

void MainWindow::on_pBond_clicked()
{
    if(sOndas->isVisible() == false) {
        sOndas->show();
        sOndas->tipo_onda=configuracion_act.onda;
    }
    else {
        sOndas->hide();
    }
}

void MainWindow::mousePress(QMouseEvent* evento)
{

    if(opcFreq->isVisible())
        opcFreq->hide();

    if(leer_estado->isActive())
        leer_estado->stop();

    if(puntitos->isActive())
        puntitos->stop();

    if(mOpc->isVisible()) {
       // ui->label_6->setVisible(false);
        mOpc->hide();
    }

    if(sOndas->isVisible())
        sOndas->hide();

  if(configuracion_act.onda == 3)
    emit llamar_act(1,evento);
}

void MainWindow::mousMove(QMouseEvent* conecs)
{
    if(configuracion_act.onda == 3)
        emit llamar_act(2,conecs);
}

void MainWindow::mousRelease(QMouseEvent* conecs)
{
    if(configuracion_act.onda == 3)
        emit llamar_act(3,conecs);
}

void MainWindow::punt()
{
    QString qstemp;
    qstemp.clear();
    qstemp+="CARGANDO";

    for(int i=0;i<punt_e;i++)
    {
        if(i<punt_e)
            qstemp+=".";
    }

    ui->label_espera->setText(qstemp);

    switch (punt_e) {
    case 0: punt_e=1;
            break;
    case 1: punt_e=2;
            break;
    case 2: punt_e=3;
            break;
    case 3: punt_e=0;
            break;
    }
}

void MainWindow::mover_graph(int estado,QMouseEvent* evento)
{ //estado , 0: nada, 1: click, 2:moviendo con click, 3:solto
    double xg,yg;

    if(estado==1) {
        if(act_estado==0) {
            act_estado=1;
            xg=0;
            yg=0;
            int i=0;

            ui->grafico->graph(0)->pixelsToCoords(evento->x(),evento->y(),xg,yg);

            if(xg<lim_tiempo) {
                for(i=0;configuracion_act.valores_arb_x[i]<=xg;i++);

                configuracion_act.valores_arb[i]=yg;

                actualizar();
            }
        }
    }

    if(estado==2) {
        if(act_estado==1 || act_estado==2) {
            act_estado=1;
            xg=0;
            yg=0;
            int i=0;

            ui->grafico->graph(0)->pixelsToCoords(evento->x(),evento->y(),xg,yg);

            if(xg<lim_tiempo) {
                for(i=0;configuracion_act.valores_arb_x[i]<=xg;i++);

                configuracion_act.valores_arb[i]=yg;

                actualizar();
            }
        }
    }

    if(estado==3) {
        if(act_estado==1 || act_estado==2) {
            act_estado=0;
        }
    }
}

void MainWindow::on_pBopc_clicked()
{
    if(mOpc->isVisible() == false) {
        leer_estado->start();
       // ui->label_6->setVisible(true);
        mOpc->show();
    }
    else{
        leer_estado->stop();
        //ui->label_6->setVisible(false);
        mOpc->hide();
    }
}

void MainWindow::on_pBdivY_clicked()
{
    if(configuracion_act.onda == 3) {
        switch (arb_rango) {
        case 10: arb_rango=2;
                break;
        case 2: arb_rango=5;
                break;
        case 5: arb_rango=10;
                break;
        }

        actualizar();
    }
}

