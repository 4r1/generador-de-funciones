﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define PI 3.14159265
#define c_muestras_1ms 3333
#define limitem_0 3
#define limites_0 200
#define limitem_1 3
#define limites_1 200
#define limitem_2 3
#define limites_2 200
#define limitem_3 3
#define limites_3 200

#include <QMainWindow>
#include <QMouseEvent>
#include "opciones.h"
#include "selondas.h"
#include "menuopc.h"
#include <bitset>
#include <QTimer>
#include <cstdlib>

typedef struct str_config {
    float vpp;
    float offset;
    float freq;
    int freq_mult;
    int onda; //0=seno,1=cuadrada,2=triangular,3=arbitraria
    QVector<double> valores_arb;
    QVector<double> valores_arb_x;

    str_config(float def_vpp=0,float def_offset=0,float def_freq=0,int def_freq_mult=0,int def_onda=0) {
        vpp=def_vpp;
        offset=def_offset;
        freq=def_freq;
        onda=def_onda;
        freq_mult=def_freq_mult;
    }

    struct str_config leer_mem(void);
    void escribir_mem(struct str_config *actual);
} config;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    config configuracion_act;
    int act_estado;
    float limx;
    float limy;
    float lim_tiempo;
    int arb_rango;
    float Vmin,Vmax;
    QFile *escritura_valores;
    QFile *escritura_valores2;
    QFile *lectura_estado_act;
    int punt_e;
    int actualizando;
    bool actualizandob;

    opciones *opcVppOffset;
    opciones *opcFreq;
    opciones *opcdivx;
    opciones *opcdivy;
    selOndas *sOndas;
    menuOpc *mOpc;
    QTimer *puntitos;
    QTimer *leer_estado;

    void actualizar(void);
    void inic_onda(void);
    void acomodar_limx(void);
    void acomodar_limy(void);
    void mod_arb(bool estado);
    float valory(double valorx);
    void definir_muestras(void);
    void verificar_limite(void);

    int c_muestras;
    int c_muestras_arb;
    int cantidad_muestras_total;

private slots:
    void on_pBVpp_clicked();
    void on_pBfreq_clicked();
    void on_pBond_clicked();
    void on_pBopc_clicked();
    void on_pBdivY_clicked();
    void mousePress(QMouseEvent* evento);
    void mousMove(QMouseEvent* conecs);
    void mousRelease(QMouseEvent* conecs);
    void mover_graph(int estado,QMouseEvent* conecm);
    void punt(void);

public slots:
    void leer_valores(int sel);
    void tyv_valores(void);
    void restablecer_arb(void);
    void leer_estado_f(void);

private:
    Ui::MainWindow *ui;

signals:
    void llamar_act(int estado, QMouseEvent* conec);
};

#endif // MAINWINDOW_H
