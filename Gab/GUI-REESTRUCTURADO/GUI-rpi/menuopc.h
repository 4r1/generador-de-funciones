#ifndef MENUOPC_H
#define MENUOPC_H

#include <QWidget>

namespace Ui {
class menuOpc;
}

class menuOpc : public QWidget
{
    Q_OBJECT

public:
    explicit menuOpc(QWidget *parent = nullptr);
    ~menuOpc();

private:
    Ui::menuOpc *ui;

signals:
    void enviar_valores(void);
    void restablecer_arb(void);
private slots:
    void on_pBAct_clicked();
    void on_pBRest_clicked();
};

#endif // MENUOPC_H
