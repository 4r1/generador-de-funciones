\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Objetivos}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Descripci\IeC {\'o}n de bloques}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Raspberry Pi}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Pantalla TFT}{2}{section.2.2}
\contentsline {section}{\numberline {2.3}Conversor Digital-Anal\IeC {\'o}gico}{2}{section.2.3}
\contentsline {section}{\numberline {2.4}Filtro activo}{3}{section.2.4}
\contentsline {section}{\numberline {2.5}Amplificador de instrumentaci\IeC {\'o}n}{3}{section.2.5}
\contentsline {section}{\numberline {2.6}Generador de se\IeC {\~n}ales: sinusoidal, cuadrada y triangular}{3}{section.2.6}
