\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Diagrama de bloques}{2}{section.1.2}
\contentsline {subsection}{Alimentaciones (Ex)}{2}{section*.3}
\contentsline {subsection}{Conexiones (Cx)}{2}{section*.4}
\contentsline {chapter}{\numberline {2}Interfaz Gr\IeC {\'a}fica}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Desarrollo}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Capturas y referencias}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Explicaci\IeC {\'o}n de referencias}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Funcionalidades principales}{7}{section.2.4}
\contentsline {section}{\numberline {2.5}Representaci\IeC {\'o}n de funciones}{7}{section.2.5}
\contentsline {subsection}{Senoidal}{7}{section*.9}
\contentsline {subsection}{Cuadrada}{8}{section*.10}
\contentsline {subsection}{Triangular}{8}{section*.11}
\contentsline {subsection}{Arbitraria}{9}{section*.12}
\contentsline {chapter}{\numberline {3}Raspberry Pi}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Codigo Assembly}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Conexiones}{11}{section.3.2}
