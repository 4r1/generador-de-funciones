const int rele = 35;
const int dacp[8] = {22, 24, 26, 28, 30, 32, 34, 36}; // MSB --> LSB
const int dacn[8] = {38, 40, 42, 45, 46, 48, 50, 52}; // MSB --> LSB

//-----------Prototipos de funciones----------------
void bin(byte);
byte mapeo(float, float, float, float, float);
void configDAC(bool, float);
//--------------------------------------------------

String recib, num;
bool digital[8] = {0, 0, 0, 0, 0, 0, 0, 0};

void setup()
{
  pinMode(rele, OUTPUT); // rele en 0, Offset Positivo.
  digitalWrite(rele, HIGH); // Se inicializa el rele en 0

  for(int i=0; i<8; i++)
  {
    pinMode(dacp[i], OUTPUT);
    pinMode(dacn[i], OUTPUT);
    digitalWrite(dacp[i], LOW);
    digitalWrite(dacn[i], LOW);
  }
  
  Serial.begin(115200);

  while(!Serial)
    ; // Espera a que se inicialice la com. serial
}

void loop()
{
  if(Serial.available())
  {
    recib = Serial.readString();

    if(recib.charAt(0) == '+')
    {
      num = recib.substring(1);
      configDAC(0, num.toFloat());
    }

    else if(recib.charAt(0) == '-')
    {
      num = recib.substring(1);
      configDAC(1, num.toFloat());
    }
  }
}

byte mapeo(float x, float in_min, float in_max, float out_min, float out_max)
{
  return round((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

void bin(byte valConv)
{
  for(int i=7; i>=0; i--)
  {
    digital[i] = valConv%2;
    valConv = valConv/2;
  }
}

void configDAC(bool tOffset, float offset)
{
  byte valor = mapeo(offset, 0.0, 5.0, 0.0, 255.0);
  
  bin(valor);
  
  if(tOffset == 0)
  {    
    digitalWrite(rele, HIGH);

    for(int i=7; i>=0; i--)
      digitalWrite(dacn[i], LOW);
      
    for(int i=7; i>=0; i--)
      digitalWrite(dacp[i], digital[i]);
  }

  else
  {
    digitalWrite(rele, LOW);
    
    for(int i=7; i>=0; i--)
      digitalWrite(dacp[i], LOW);
    
    for(int i=7; i>=0; i--)
      digitalWrite(dacn[i], digital[i]);
  }
}
