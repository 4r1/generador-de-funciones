# Generador de Funciones Arbitrarias

Diseño completo y libre (Software y Hardware disponibles en este repositorio) de generador de funciones capaz de entregar en su salida señales arbitrarias ingresadas mediante dibujo en pantalla táctil.

![](https://gitlab.com/4r1/generador-de-funciones/raw/master/logo/logoTeam.png)
