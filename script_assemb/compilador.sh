#!/usr/bin/env bash
sudo echo "Sudo adquirido"
quilal=$(ps a|grep ejecutable|wc -l)
if [ "$quilal" == "1" ]
then
	echo "No habia ejecutables corriendo anteriormente"
else
	sudo killall ejecutable
	echo "Mate todos los ejecutables viejos"
fi
guail=$(ps a|grep compilador.sh|wc -l)
while [ "$guail" != "3" ]
do
	peidrepe=$(ps a|grep compilador.sh|cut -d$'\n' -f1| cut -d' ' -f1)
	if [ "$peidrepe" == "$$" ]
	then
		echo "Soy el unico compilador.sh corriendo"
	else
		sudo kill -9 $peidrepe
		echo "Intento matar un compilador repetido"
		echo $$
		echo $peidrepe
	fi
	guail=$(ps a|grep compilador.sh|wc -l)
	echo $guail
done
echo "Si el proceso se lanzo varias veces por las dudas intente matar los anteriores"

datos1='	.arch armv6
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.file	"adc.c"
	.text
	.align	2
	.global	pioInit
	.syntax unified
	.arm
	.fpu vfp
	.type	pioInit, %function
pioInit:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r4, lr}
	sub	sp, sp, #8
	ldr	r1, .L3
	ldr	r0, .L3+4
	bl	open
	ldr	r3, .L3+8
	mov	r2, #3
	str	r3, [sp, #4]
	mov	r1, #4096
	mov	r3, #1
	mov	r4, r0
	str	r0, [sp]
	mov	r0, #0
	bl	mmap
	ldr	r3, .L3+12
	str	r0, [r3]
	mov	r0, r4
	add	sp, sp, #8
	@ sp needed
	pop	{r4, lr}
	b	close
.L4:
	.align	2
.L3:
	.word	1052674
	.word	.LC0
	.word	1059061760
	.word	gpio
	.size	pioInit, .-pioInit
	.align	2
	.global	pinMode
	.syntax unified
	.arm
	.fpu vfp
	.type	pinMode, %function
pinMode:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L7
	ldr	r2, .L7+4
	str	lr, [sp, #-4]!
	smull	ip, r3, r3, r0
	ldr	r2, [r2]
	asr	ip, r0, #31
	rsb	r3, ip, r3, asr #2
	mvn	lr, r1
	add	ip, r3, r3, lsl #2
	and	lr, lr, #7
	sub	r0, r0, ip, lsl #1
	ldr	ip, [r2, r3, lsl #2]
	add	r0, r0, r0, lsl #1
	and	r1, r1, #7
	bic	ip, ip, lr, lsl r0
	str	ip, [r2, r3, lsl #2]
	ldr	ip, [r2, r3, lsl #2]
	orr	r1, ip, r1, lsl r0
	str	r1, [r2, r3, lsl #2]
	ldr	pc, [sp], #4
.L8:
	.align	2
.L7:
	.word	1717986919
	.word	gpio
	.size	pinMode, .-pinMode
	.align	2
	.global	digitalWrite
	.syntax unified
	.arm
	.fpu vfp
	.type	digitalWrite, %function
digitalWrite:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	asr	r2, r0, #31
	cmp	r0, #0
	lsr	r2, r2, #27
	add	r3, r0, #31
	movge	r3, r0
	cmp	r1, #0
	add	r0, r0, r2
	ldr	r1, .L13
	and	r0, r0, #31
	sub	r0, r0, r2
	mov	r2, #1
	asr	r3, r3, #5
	ldr	r1, [r1]
	addne	r3, r3, #7
	addeq	r3, r3, #10
	lsl	r0, r2, r0
	str	r0, [r1, r3, lsl #2]
	bx	lr
.L14:
	.align	2
.L13:
	.word	gpio
	.size	digitalWrite, .-digitalWrite
	.section	.text.startup,"ax",%progbits
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu vfp
	.type	main, %function
main:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r7, lr}
	bl	pioInit
	ldr	r3, .L18
	mov	r1, #64
	mov	ip, #1
	ldr	r3, [r3]
	mov	r6, #524288
	mov	r5, #65536
	ldr	r2, [r3, #4]
	mov	r4, #4096
	bic	r2, r2, #805306368
	str	r2, [r3, #4]
	ldr	r2, [r3, #4]
	mov	lr, #2
	orr	r2, r2, #134217728
	str	r2, [r3, #4]
	ldr	r2, [r3, #4]
	mov	r0, #32
	bic	r2, r2, #1572864
	str	r2, [r3, #4]
	ldr	r7, [r3, #4]
	mov	r2, #8192
	orr	r7, r7, #262144
	str	r7, [r3, #4]
	ldr	r7, [r3, #4]
	bic	r7, r7, #384
	str	r7, [r3, #4]
	ldr	r7, [r3, #4]
	orr	r7, r7, r1
	str	r7, [r3, #4]
	ldr	r7, [r3]
	bic	r7, r7, #48
	str	r7, [r3]
	ldr	r7, [r3]
	orr	r7, r7, #8
	str	r7, [r3]
	ldr	r7, [r3]
	bic	r7, r7, #6
	str	r7, [r3]
	ldr	r7, [r3]
	orr	r7, r7, ip
	str	r7, [r3]
	ldr	r7, [r3]
	bic	r7, r7, #196608
	str	r7, [r3]
	ldr	r7, [r3]
	orr	r7, r7, #32768
	str	r7, [r3]
	ldr	r7, [r3]
	bic	r7, r7, #1572864
	str	r7, [r3]
	ldr	r7, [r3]
	orr	r7, r7, #262144
	str	r7, [r3]
	ldr	r7, [r3, #4]
	bic	r7, r7, #3072
	str	r7, [r3, #4]
	ldr	r7, [r3, #4]
	orr	r7, r7, #512
	str	r7, [r3, #4]
	b	.L19
.L20:
	.align	2
.L18:
	.word	gpio
.L19:
.L16:'

datos2='	b	.L16
	.size	main, .-main
	.comm	gpio,4,4
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"/dev/mem\000"
	.ident	"GCC: (Raspbian 6.3.0-18+rpi1+deb9u1) 6.3.0 20170516"
	.section	.note.GNU-stack,"",%progbits'
	
echo "$datos1" > programa.s
for x in $(cat datos.txt); do 
	for y in $(seq 0 7); do
		if [ "${x:y:1}" == "0" ]; then
			case $y in 
				7) echo -e "\tstr\t r6, [r3, #40]" >> programa.s; ;;
				6) echo -e "\tstr\t r5, [r3, #40]" >> programa.s; ;;
				5) echo -e "\tstr\t r4, [r3, #40]" >> programa.s; ;;
				4) echo -e "\tstr\t lr, [r3, #40]" >> programa.s; ;;
				3) echo -e "\tstr\t ip, [r3, #40]" >> programa.s; ;;
				2) echo -e "\tstr\t r0, [r3, #40]" >> programa.s; ;;
				1) echo -e "\tstr\t r1, [r3, #40]" >> programa.s; ;;
				0) echo -e "\tstr\t r2, [r3, #40]\n" >> programa.s; ;;
			esac
		else
			case $y in 
				7) echo -e "\tstr\t r6, [r3, #28]" >> programa.s; ;;
				6) echo -e "\tstr\t r5, [r3, #28]" >> programa.s; ;;
				5) echo -e "\tstr\t r4, [r3, #28]" >> programa.s; ;;
				4) echo -e "\tstr\t lr, [r3, #28]" >> programa.s; ;;
				3) echo -e "\tstr\t ip, [r3, #28]" >> programa.s; ;;
				2) echo -e "\tstr\t r0, [r3, #28]" >> programa.s; ;;
				1) echo -e "\tstr\t r1, [r3, #28]" >> programa.s; ;;
				0) echo -e "\tstr\t r2, [r3, #28]\n" >> programa.s; ;;
			esac
		fi
	done
done
echo "$datos2" >> programa.s
gcc programa.s -o ejecutable
echo 'Compile el ejecutable'
sudo chmod +x ejecutable
echo 'Le di permisos de ejecucion'
sudo nice -n -20 sudo ./ejecutable &
echo 'Lo comence a ejecutar'
peid1="$(ps a|grep "./ejecutable"|cut -d$'\n' -f1| cut -d' ' -f1)"
echo "El PID del proceso que ejecute es: $peid1"
peid2="$(ps a|grep "./ejecutable"|cut -d$'\n' -f2| cut -d' ' -f1)"
echo "El PID del proceso hijo es: $peid2"
peid3="$(ps a|grep "./ejecutable"|cut -d$'\n' -f3| cut -d' ' -f1)"
echo "El PID del proceso hijo del hijo es: $peid3"
sudo taskset -cp 2 $peid1
sudo taskset -cp 2 $peid2
sudo taskset -cp 3 $peid3
echo 'Configure el proceso para que se ejecute solo en el tercer nucleo'
