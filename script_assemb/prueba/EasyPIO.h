#ifndef easyPIOlib
#define easyPIOlib

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

//GPIO preproc
#define BCM2837_PERI_BASE 	0x3F000000
#define GPIO_BASE 			(BCM2837_PERI_BASE + 0x200000)
volatile unsigned int *gpio;
#define GPFSEL ((volatile unsigned int *) (gpio + 0))
#define GPSET ((volatile unsigned int *) (gpio + 7))
#define GPCLR ((volatile unsigned int *) (gpio + 10))
#define GPLEV ((volatile unsigned int *) (gpio + 13))
#define GPLEV0 				(* (volatile unsigned int *) (gpio + 13))
#define BLOCK_SIZE			(4*1024)

void pioInit(void);
void pinMode(int, int);
void digitalWrite(int, int);

#endif

void pioInit () {
	int mem_fd;
	void *reg_map;
	mem_fd = open("/dev/mem", O_RDWR|O_SYNC); // abre /dev/mem
	reg_map = mmap(
		NULL,					// dir. donde comienza el mapeo (null=no importa)
		BLOCK_SIZE,				// 4KB bloque mapeado
		PROT_READ|PROT_WRITE, 	// permite lect.y escr.en la mem.
		MAP_SHARED,				// acceso no exclusivo
		mem_fd,					// puntero a /dev/mem
		GPIO_BASE);				// offset a la GPIO
	gpio = (volatile unsigned *)reg_map;

	close(mem_fd);	
}

void pinMode(int pin, int funcion) {
	int reg = pin/10;
	int offset = (pin%10) *3;

	GPFSEL[reg] &= ~((0b111 & ~funcion) << offset);
	GPFSEL[reg] |= ((0b111 & funcion) << offset);
}

void digitalWrite(int pin, int val) {
	int reg = pin / 32;
	int offset = pin % 32;

	if (val)
		GPSET[reg] = 1 << offset;
	else
		GPCLR[reg] = 1 << offset;
}
