#include "EasyPIO.h"

void main(void){
	pioInit();
	pinMode(6 ,1);
	pinMode(12,1);
	pinMode(13,1);
	pinMode(16,1);
	pinMode(19,1);
	pinMode(20,1);
	pinMode(21,1);
	pinMode(26,1);

	while(1){
		digitalWrite(6 ,0);
		digitalWrite(12,0);
		digitalWrite(13,0);
		digitalWrite(16,0);
		digitalWrite(19,0);
		digitalWrite(20,0);
		digitalWrite(21,0);
		digitalWrite(26,0);

		digitalWrite(6 ,1);
		digitalWrite(12,1);
		digitalWrite(13,1);
		digitalWrite(16,1);
		digitalWrite(19,1);
		digitalWrite(20,1);
		digitalWrite(21,1);
		digitalWrite(26,1);
	}
}
